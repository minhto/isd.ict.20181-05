# 1. Front end Architectural Pattern
## a. Framework: Angular 2
## b. Programming languages: TypeScript, JavaScript, HTML, CSS
# 2. Software Architecture
## a. Original: Component base pattern
<<<<<<< HEAD


=======
## b. Implementation:
### i. Main components of this architecture
In this desmontration all the source files will be place under the **angular-src** folder.
* The entry point of the application will be the **index.html** file.
![Image1](./Images/Image1.png)
In this file there is one **Angular Component**, which is named *app-root*.
* All the components are defined under folder *src/app*. Each component consists of one *template* .html file, several *style sheet* .css, and a *component* .ts file. The *app-root* component are defined in the *app.component.ts* file.
![Image2](./Images/Image2.png)
Inside the *template* *app.component.html*, we can see that several sub components are defined here since Angular follow the components base design.
![Image3](./Images/Image3.png)
* *Router-outlet* is a special component, it will be replace by a component depends on the current route displayed in the browser url. These components will be defined under *src/app/components*. Here is the *component.ts* file of the *login view*.
![Image4](./Images/Image4.png)
Here is the *template* .html file of the *login view*.
![Image5](./Images/Image5.png)
This template consists of a simple login form and a submit button which upon clicked will call the *onLoginSubmit()* function inside the *component.ts* file.
* All the functionality which are server related are put inside a *service.ts* file instead of directly inside a *component.ts* file. This ensure the reusability of these functions. All the *service.ts* file are places under *src/app/services* folder. This is the *user.service.ts* file.
![Image6](./Images/Image6.png)
As in this figure, this service contain a *authenticateUser()* function, which upon invoked will send a *POST request* to the server api at */users/authenticate*.
### ii. Main flow of the architecture
* The *template* .html files play the role of the *boundary class* in this model. Those are the only thing the user can directly interacts with. The logic of these pages are defined inside each corresponse *component.ts* file.
* In order to interact with the server side, a component can injects a service and uses its functionality. This ensure code reusability.
* *For example*: Main flow of a login use-case
    * *1.* The user select the login page.
    * *2.* The *login* componet will be displayed where the *router-outlet* component should be.
    * *3.* The user fills in the form and hit the submit button.
    * *4.* The function *onLoginSubmit()* inside the *login.component.ts* file is called. This function and then call the *authenticateUser()* function inside the *user.service.ts* file.
    * *5.* A *POST request* is sent to the server api at */users/authenticate*, upon a response is returned, its content will be parsed and displays to the user interface.
>>>>>>> cecce48ead120312b4ca0476e22d97ae90811b9f
