# 1. Back end Architectural Pattern
## a. Framework: Django REST Framework
## b. Programming languages: Python
# 2. Software Architecture
## a. Django follows MVC pattern but has some modifications. In particular, MTV pattern is the precise term (Model - Template - View), View plays the role of the controller in MVC, while Template plays the role of View in MVC, but in Django REST Framework, we do not deal with Template.

## b. Implementation
### i. Main components of this architecture 

The project will be composed of separated apps, in which there will always be a default app, where you set up the URL configuration in *url.py*  

![Image1](./pictures/Image1.png)

and modify database and installed apps, as well as packages used in *settings.py*

![Image2](./pictures/Image4.png)

The api app will send response or handle requests sent from the front-end, which will be in JSON form. This is where *serializers.py* come in. Serializers allow complex data such as querysets and model instances to be converted to native Python datatypes that can then be easily rendered into JSON.

![Image3](./pictures/Image2.png)

The View in api app: Handle methods work with REST framework's Response and Request, not Django's HttpResponse/Request, otherwise it's pretty much similar to traditional Django views.
Moreover, the framework also provides you with generic views, allow you to quickly build API views that map closely to your database models.

![Image4](./pictures/Image3.png)


