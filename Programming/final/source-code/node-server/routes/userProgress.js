const express = require("express");
const router = express.Router();
const UserProgress = require("../models/userProgress");

router.get('/list', (req, res, next) => {
    UserProgress.GetAllUserProgresses((err, progresses) => {
        if (err) {
            res.send(`Error occured: ${err}`);
            next();
        }
        res.json(progresses);
    })
})

router.put('/update', (req, res, next) => {
    UserProgress.UpdateUserProgress(req.body.userID, req.body.progresses, (err) => {
        if (err) {
            res.json({ success: false, msg: "Failed to update user progress"})
        } else {
            res.json({ success: true, msg: "Successfully update user progress" })
        }
    })
})

router.put('/updateBadges', (req, res, next) => {
    UserProgress.UpdateBadge(req.body.m_sUserID, req.body.m_asBadges, (err) => {
        if (err) {
            res.json({ success: false, msg: "Failed to update user badges"})
        } else {
            res.json({ success: true, msg: "Successfully update user badges" })
        }
    })
})

module.exports = router;