const mongoose = require("mongoose");

const ProgressChema = mongoose.Schema({
    m_sCourseUrl: String,
    m_sProgress: String
})

const UserProgressSchema = mongoose.Schema({
    m_sUserID: String,
    m_iExp: Number,
    m_asBadges: [String],
    m_apProgresses: [ProgressChema]
})

const UserProgress = module.exports = mongoose.model('UserProgress', UserProgressSchema);

module.exports.GetAllUserProgresses = (callback) => {
    UserProgress.find({}, callback);
}

module.exports.NewUserProgress = (progress, callback) => {
    progress.save(callback);
}

module.exports.GetUserProgressByUserID = (id, callback) => {
    UserProgress.findOne({m_sUserID: id}, callback);
}

module.exports.UpdateUserProgress = (userID, progresses, callback) => {
    UserProgress.update({m_sUserID: userID}, {m_apProgresses: progresses}, callback);
}

module.exports.UpdateBadge = (userID, badges, callback) => {
    UserProgress.update({m_sUserID: userID}, {m_asBadges: badges}, callback);
}