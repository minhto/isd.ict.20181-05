import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { UserService } from '../services/user.service';
import { User } from '../classes/user';

describe('User Service test cases', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('User service creation success', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  describe('Login Use Case test cases', () => {
    describe('+ Test case: No username', () => {
      it('should return message: "Username is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        
        service.AuthenticateUser(user).subscribe(data => {
          expect(data.msg).toEqual("Username is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, No password', () => {
      it('should return message: "Password is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        
        service.AuthenticateUser(user).subscribe(data => {
          expect(data.msg).toEqual("Password is empty");
          done();
        })
      });
    });

    describe('+ Test case: All fields valid, user does not exist', () => {
      it('should return authentication result: fail', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        user.m_sPassword = "tset";
        
        service.AuthenticateUser(user).subscribe(data => {
          expect(data.success).toEqual(false);
          done();
        })
      });
    });

    describe('+ Test case: All fields valid, user does exist', () => {
      it('should return authentication result: success', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        user.m_sPassword = "test";
        
        service.AuthenticateUser(user).subscribe(data => {
          expect(data.success).toEqual(true);
          done();
        })
      });
    });
  })

  describe('Register Use Case test cases', () => {
    describe('+ Test case: No username', () => {
      it('should return message: "Username is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        
        service.RegisterUser(user).subscribe(data => {
          expect(data.msg).toEqual("Username is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, No password', () => {
      it('should return message: "Password is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        
        service.RegisterUser(user).subscribe(data => {
          expect(data.msg).toEqual("Password is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, Has password, No email', () => {
      it('should return message: "Email is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        user.m_sPassword = "test";
        
        service.RegisterUser(user).subscribe(data => {
          expect(data.msg).toEqual("Email is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, Has password, Has invalid email', () => {
      it('should return message: "Email format is invalid"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        user.m_sPassword = "test";
        user.m_sEmail = "test";
        
        service.RegisterUser(user).subscribe(data => {
          expect(data.msg).toEqual("Email format is invalid");
          done();
        })
      });
    });

    describe('+ Test case: All valid field, register using existed info', () => {
      it('should return register result: fail', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = new User();
        user.m_sUsername = "test";
        user.m_sPassword = "test";
        user.m_sEmail = "test@gmail.com";
        
        service.RegisterUser(user).subscribe(data => {
          expect(data.success).toEqual(false);
          done();
        })
      });
    });

    // describe('+ Test case: All valid field, register with new info', () => {
    //   it('should return register result: success', (done: DoneFn) => {
    //     const service: UserService = TestBed.get(UserService);
    //     const user = new User();
    //     user.m_sUsername = "test1";
    //     user.m_sPassword = "test";
    //     user.m_sEmail = "test@gmail.com";
        
    //     service.RegisterUser(user).subscribe(data => {
    //       expect(data.success).toEqual(true);
    //       done();
    //     })
    //   });
    // });
  })

  describe('Reset User Password test cases', () => {
    describe('+ Test case: No email address was sent', () => {
      it('should return message: "Email is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {};
        
        service.ResetUserPassword(user).subscribe(data => {
          expect(data.msg).toEqual("Email is empty");
          done();
        })
      });
    });

    describe('+ Test case: Invalid email format', () => {
      it('should return message: "Invalid email format"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          m_sEmail: 'sample-emailgmail.com'
        }
        
        service.ResetUserPassword(user).subscribe(data => {
          expect(data.msg).toEqual("Invalid email format");
          done();
        })
      });
    });

    describe('+ Test case: Valid email format, but no user has that email', () => {
      it('should return message: "There is no user with this email"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          m_sEmail: 'sample-email@gmail.com'
        }
        
        service.ResetUserPassword(user).subscribe(data => {
          expect(data.msg).toEqual("There is no user with this email");
          done();
        })
      });
    });

    // describe('+ Test case: Valid email format, there is an user with that email', () => {
    //   it('should return result message: success', (done: DoneFn) => {
    //     const service: UserService = TestBed.get(UserService);
    //     const user = {
    //       m_sEmail: 'tuannguyendinh224@gmail.com'
    //     }
        
    //     service.ResetUserPassword(user).subscribe(data => {
    //       console.log(data);
    //       expect(data.success).toEqual(true);
    //       done();
    //     })
    //   });
    // });
  });

  describe('Change User Password test cases', () => {
    describe('+ Test case: No username', () => {
      it('should return message: "Username is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {};
        
        service.ChangePassword(user).subscribe(data => {
          expect(data.msg).toEqual("Username is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, No old password', () => {
      it('should return message: "Old Password is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          username: 'test'
        };
        
        service.ChangePassword(user).subscribe(data => {
          expect(data.msg).toEqual("Old Password is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has username, Has old password, No new password', () => {
      it('should return message: "New Password is empty"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          username: 'test',
          oldPassword: 'test'
        };
        
        service.ChangePassword(user).subscribe(data => {
          expect(data.msg).toEqual("New Password is empty");
          done();
        })
      });
    });

    describe('+ Test case: Has all fields, but username/password not match', () => {
      it('should return message: "Wrong password"', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          username: 'test',
          oldPassword: 'tset',
          newPassword: 'test'
        };
        
        service.ChangePassword(user).subscribe(data => {
          expect(data.msg).toEqual("Wrong password");
          done();
        })
      });
    });

    describe('+ Test case: Has all valid fields, change password success', () => {
      it('should return result message: success', (done: DoneFn) => {
        const service: UserService = TestBed.get(UserService);
        const user = {
          username: 'test',
          oldPassword: 'test',
          newPassword: 'test'
        };
        
        service.ChangePassword(user).subscribe(data => {
          expect(data.success).toEqual(true);
          done();
        })
      });
    });
  });

  describe('Flag variables test cases', () => {
    describe('+ Get "isAdmin" flag', () => {
      it('should return store "isAdmin" flag', () => {
        const service: UserService = TestBed.get(UserService);
        expect(service.IsAdmin()).toEqual(!localStorage.getItem('isAdmin') == null);
      });
    });
    
    describe('+ Get "isLoggedIn" flag', () => {
      it('should return stored username', () => {
        const service: UserService = TestBed.get(UserService);
        expect(service.IsLoggedIn()).toEqual(!localStorage.getItem('username') == null);
      });
    });
  });
});
