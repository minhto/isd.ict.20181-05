import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { NotadminGuard } from '../guards/notadmin.guard';

describe('NotadminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientModule ],
      providers: [NotadminGuard]
    });
  });

  it('should ...', inject([NotadminGuard], (guard: NotadminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
