import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { NotauthGuard } from '../guards/notauth.guard';

describe('NotauthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientModule ],
      providers: [NotauthGuard]
    });
  });

  it('should ...', inject([NotauthGuard], (guard: NotauthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
