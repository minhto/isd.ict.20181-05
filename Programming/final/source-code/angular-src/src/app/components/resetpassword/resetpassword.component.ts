import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UIService } from '../../services/ui.service';

declare var require: any;
const cf_Messages = require("../../../assets/messages.json").resetpassword;

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  private m_sEmail: String;

  constructor(
    private router: Router,
    private userService: UserService,
    private uiService: UIService
    ) { }

  ngOnInit() {
  }

  private OnResetSubmit() {
    if (!this.ValidateEmail(this.m_sEmail)) {
      this.uiService.OpenModal(cf_Messages.invalid_email.en, cf_Messages.invalid_email_message.en);
    } else {
      this.uiService.ShowButtonLoading('resetBtn');
      
      const user = {
        m_sEmail: this.m_sEmail
      }

      setTimeout(() => {
        this.userService.ResetUserPassword(user).subscribe(data => {
          this.uiService.StopButtonLoading('resetBtn');

          if (data.success) {
            this.uiService.OpenModal(cf_Messages.success.en, data.msg);
            this.router.navigate(['login']);
          } else {
            this.uiService.OpenModal(cf_Messages.fail.en, data.msg);
          }
        })
      }, 1000);
    }
  }

  private ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
