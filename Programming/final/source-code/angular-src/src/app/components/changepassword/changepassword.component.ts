import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UIService } from '../../services/ui.service';

declare var require: any;
const cf_Messages = require("../../../assets/messages.json").changepassword;

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  private sOldPassword: String;
  private sNewPassword: String;
  private sConfirmPassword: String;

  constructor(
    private router: Router,
    private userService: UserService,
    private uiService: UIService
  ) { }

  ngOnInit() {
  }

  private OnChangeSubmit() {
    if (!this.sOldPassword) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.empty_old_password.en);
    } else if (!this.sNewPassword) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.empty_new_password.en);
    } else if (this.sNewPassword != this.sConfirmPassword) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.not_confirm_password.en);
    } else {
      this.userService.GetUsername().then(username => {
        this.uiService.ShowButtonLoading('changePasswordBtn');

        setTimeout(() => {
          this.userService.ChangePassword({username: username, oldPassword: this.sOldPassword, newPassword: this.sNewPassword}).subscribe(data => {
            this.uiService.StopButtonLoading('changePasswordBtn');

            if (data.success) {
              this.sOldPassword = null;
              this.sNewPassword = null;
              this.sConfirmPassword = null;
              this.uiService.OpenModal(cf_Messages.success.en, cf_Messages.success_message.en);
              this.userService.Logout();
              this.router.navigate(['/login']);
            } else {
              this.uiService.OpenModal(cf_Messages.fail.en, cf_Messages.fail_message.en);
            }
          })
        }, 1000);
      })
    }
  }
}
