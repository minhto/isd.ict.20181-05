import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UIService } from '../../services/ui.service';

import { User } from '../../classes/user';

declare var require: any;
const cf_Messages = require("../../../assets/messages.json").login;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private m_uUser: User;

  constructor(
    private router: Router,
    private userService: UserService,
    private uiService: UIService
  ) { }

  ngOnInit() {
    this.m_uUser = new User();
  }

  private OnLoginSubmit() {
    this.uiService.ShowButtonLoading('loginBtn');

    // Show loading animation for 1 sec
    setTimeout(() => {
      this.userService.AuthenticateUser(this.m_uUser).subscribe(data => {
        this.uiService.StopButtonLoading('loginBtn');
        
        if (data.success) {
          if (!data.admin) {
            this.userService.StoreUserProgress(data.progress);
          } else {
            this.userService.StoreUserPriviledge(data.admin);
          }
          this.userService.StoreUsername(this.m_uUser.m_sUsername);
          this.router.navigate(['home']);
        } else {
          this.uiService.OpenModal(cf_Messages.login_failed.en, data.msg);
        }
      });
    }, 1000);
  }

}
