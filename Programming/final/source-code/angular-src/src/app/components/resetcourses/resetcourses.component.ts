import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';
import { UIService } from '../../services/ui.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

declare var require: any;
const cf_Messages = require("../../../assets/messages.json").resetcourses;

@Component({
  selector: 'app-resetcourses',
  templateUrl: './resetcourses.component.html',
  styleUrls: ['./resetcourses.component.css']
})
export class ResetcoursesComponent implements OnInit {
  private m_uUserProgress: UserProgress;
  private m_acCourseData: [Course];
  private m_cCurrentCourse: Course;

  constructor(
    private router: Router,
    private userService: UserService,
    private courseService: CourseService,
    private uiService: UIService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.userService.GetUserProgress().then(userProgress => {
        this.m_uUserProgress = <UserProgress>userProgress;
        this.courseService.RequestCourses().subscribe(courseData => {
          this.m_acCourseData = <[Course]>courseData;
        })
      })
    }, 1000);
  }

  private OpenModal(course: Course) {
    this.m_cCurrentCourse = course;
  }

  private OpenCourse(course: Course) {
    this.courseService.SetCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  private OpenTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (takenCourse.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        this.OpenCourse(this.m_acCourseData[i]);
        break;
      }
    }
  }

  private GetModuleName(course: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (course.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        for (let j = 0; j < this.m_acCourseData[i].m_acModules.length; j++) {
          if (course.m_sProgress == this.m_acCourseData[i].m_acModules[j].m_sUrl) {
            return this.m_acCourseData[i].m_acModules[j].m_sName;
          }
        }
        break;
      }
    }
  }

  private ResetTakenCourse() {
    if (!this.m_cCurrentCourse) return;
    for (let i = 0; i < this.m_uUserProgress.m_apProgresses.length; i++) {
      if (this.m_cCurrentCourse.m_sCourseUrl == this.m_uUserProgress.m_apProgresses[i].m_sCourseUrl) {
        this.m_uUserProgress.m_apProgresses.splice(i, 1);
        this.userService.UpdateUserProgress(this.m_uUserProgress).subscribe(() => {
          this.uiService.OpenModal(cf_Messages.reset_success.en, cf_Messages.reset_success_message.en);
        });
        break;
      }
    }
  }
}
