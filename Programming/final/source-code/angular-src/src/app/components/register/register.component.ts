import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UIService } from '../../services/ui.service';

import { User } from '../../classes/user';

declare var require: any;
const cf_Messages = require("../../../assets/messages.json").register;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private m_uUser: User;
  private m_sConfirmPassword: String;

  constructor(
    private router: Router,
    private userService: UserService,
    private uiService: UIService
  ) { }

  ngOnInit() {
    this.m_uUser = new User();
  }

  private OnRegisterSubmit() {
    if (!this.m_uUser.m_sUsername) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.empty_username.en);
    } else if (!this.m_uUser.m_sPassword) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.empty_password.en);
    } else if (this.m_uUser.m_sPassword != this.m_sConfirmPassword) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.not_confirm_password.en);
    } else if (!this.ValidateEmail(this.m_uUser.m_sEmail)) {
      this.uiService.OpenModal(cf_Messages.invalid_field.en, cf_Messages.invalid_email.en);
    } else {
      this.uiService.ShowButtonLoading('registerBtn');
  
      setTimeout(() => {
        this.userService.RegisterUser(this.m_uUser).subscribe(data => {
          this.uiService.StopButtonLoading('registerBtn');

          if (data.success) {
            this.uiService.OpenModal(cf_Messages.register_success.en, cf_Messages.register_success_message.en);
            this.router.navigate(['login']);
          } else {
            this.uiService.OpenModal(cf_Messages.register_fail.en, data.msg);
          }
        })
      }, 1000);
    }
  }

  private ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}