# Coding convention
## Variable naming
|Prefix|Meaning|
|-|-|
|m_|class member|
|s_|static|
|str|string|
|i|int|
|is/was/has/...|boolean|
|ax|array of x|

* In some cases where a variable's name is meaningful enough, or when it is an one time used local variable with short live time in a small scope, there is **no** need to add a redundant prefix for it.

* After the prefix, variables name will be written in camel case.

* All letters in a constant's name will be capitalized.

Examples:
```
private m_strCourseName: String;
const static SCREEN_WIDTH: Int;
private m_iCurrentIndex: Int;
```
## Method naming
Capitalize the first letter of each word.

Examples:
```
OpenNextModule();
GetCourse(coursename: String);
LogOut();
```
## Comments
### Tags
Certain tags are used in comments to assist in indexing common issues. Such
tags can be searched with common programming tools, such as the UNIX
grep utility or can be filtered by some IDE such as Eclipse.
|Tag|Meaning|
|-|-|
|TODO|Used to indicate planned enhancements|
|FIXME|Used to mark potential problematic code that requires special attention and/or review|
|NOTE|Used to document inner workings of code and indicate potential pitfalls|
|BUGFIX|Used to indicate that the piece of code is added/modified in order to fix some bug|
## Commit message
|Prefix|Meaning|
|-|-|
|+|New files/features/... were added|
|-|Redundant files/methods/variables/... were removed|
|*|Bug fixed, follow by bug description|
|!|Important milestone, such as a new build/release|

Examples:
```
git commit -m "* Fixed distorted screen on resize smaller than 800*600."

git commit -m "! Week9 build release success"

git commit -m "- Remove unused import modules"
```