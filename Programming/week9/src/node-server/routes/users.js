const express = require("express");
const router = express.Router();
const User = require("../models/user");
const UserProgress = require("../models/userProgress");

const nodemailer = require("nodemailer");
const mailConfig = require("../config/mail.json");

router.get('/list', (req, res, next) => {
    User.GetAllUsers((err, users) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(users);
    })
})

router.post('/register', (req, res, next) => {
    let newUser = new User({
        m_sUsername: req.body.m_sUsername,
        m_sPassword: req.body.m_sPassword,
        m_sEmail: req.body.m_sEmail,
        m_isAdmin: false
    });

    User.GetUserWithUsername(newUser.m_sUsername, (err, user) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        } else {
            if (!user) {
                User.AddUser(newUser, (err, user) => {
                    if (err)
                        res.json({
                            success: false,
                            msg: `Error occurred: ${err}`
                        });
                    else
                        res.json({
                            success: true,
                            msg: "User resgitered successful"
                        });
                })
            } else {
                res.json({
                    success: false,
                    msg: `User already exists.`
                })
            }
        }
    })
})

router.post('/authenticate', (req, res, next) => {
    const username = req.body.m_sUsername;
    const password = req.body.m_sPassword;

    User.AuthenticateUser(username, password, (err, user) => {
        if (err) res.send(`Error occurred: ${err}`);
        if (!user)
            res.json({
                success: false,
                msg: "Wrong username or password"
            })
        else {
            if (!user.m_isAdmin) {
                UserProgress.GetUserProgressByUserID(user._id, (err, progress) => {
                    if (err) res.send(`Error occurred: ${err}`);
                    if (!progress) {
                        let newUserProgress = new UserProgress({
                            m_sUserID: user._id,
                            m_iExp: 0,
                            m_asBadges: [],
                            m_apProgresses: []
                        })
                        UserProgress.NewUserProgress(newUserProgress, (err) => {
                            if (err)
                                res.json({
                                    success: false,
                                    msg: `Error occurred: ${err}`
                                });
                            else
                                res.json({
                                    success: true,
                                    admin: user.m_isAdmin,
                                    progress: newUserProgress
                                })
                        })
                    } else
                        res.json({
                            success: true,
                            admin: user.m_isAdmin,
                            progress: progress
                        })
                })
            } else {
                res.json({
                    success: true,
                    admin: user.m_isAdmin
                })
            }
        }
    })
})

router.put('/changePassword', (req, res, next) => {
    User.AuthenticateUser(req.body.username, req.body.oldPassword, (err, user) => {
        if (err) res.send(`Error occured: ${err}`);
        if (!user) {
            res.json({ success: false, msg: "Wrong password" });
        } else {
            User.ChangePassword(req.body.username, req.body.newPassword, (err) => {
                if (err) {
                    res.json({ success: false, msg: "Failed to change user password" })
                } else {
                    res.json({ success: true, msg: "Successfully changed user password" });
                }
            })
        }
    })
})

router.post('/resetPassword', (req, res, next) => {
    User.GetUserWithEmail(req.body.email, (err, user) => {
        if (err) res.send(`Error occured: ${err}`);
        if (!user) {
            res.json({ success: false, msg: "There is no user with this email" });
        } else {
            User.ChangePassword(user.m_sUsername, user.m_sUsername, (err) => {
                if (err) {
                    res.json({ success: false, msg: "Failed to reset user password" });
                } else {
                    // Send email to user
                    let transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: mailConfig.user,
                            pass: mailConfig.pass
                        }
                    })

                    let mailOptions = {
                        from: mailConfig.user,
                        to: user.m_sEmail,
                        subject: 'Resetting password',
                        text: 'Your password has been set to your username. Please change your password right away.'
                    }

                    transporter.sendMail(mailOptions, (err, info) => {
                        if (err) {
                            res.send(`Error occured: ${err}`)
                        } else {
                            res.json({
                                success: true,
                                msg: "Successfully reset user password. Please check your email."
                            })
                        }
                    })
                }
            })
        }
    })
})

module.exports = router;