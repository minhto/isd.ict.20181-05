import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-tryit',
  templateUrl: './tryit.component.html',
  styleUrls: ['./tryit.component.css']
})
export class TryitComponent implements OnInit {

  constructor(
    /**
     * This object is used to send request to course apis.
     */
    private courseService: CourseService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * First, the application url will be retrieved.
   * From this the application can extract the file that
   * is going to be display in the code playground.
   * 
   * After that, this filename will be passed to the GetModule()
   * function of courseService.
   * 
   * After the content of the file is retrieved, the InitiateEditor()
   * function are called to set up the code playground and display the
   * value of the requested file.
   * 
   * If the filename is invalid or empty, the code playground
   * will be initiated with no content.
   */
  ngOnInit() {
    let filename = document.location.href.split('=')[1];
    if (filename !== undefined) {
      let filepath = `tryit/${filename.split('_')[0].slice(3)}/${filename}`;
      this.courseService.GetModule(filepath).toPromise().then(res => {
        this.InitiateEditor('`' + res.text() + '`');
      }).catch(err => {
        // Handle: file does not exist
        console.log("File does not exists");
        this.InitiateEditor(`''`);
      })
    } else {
      // Empty filename
      this.InitiateEditor(`''`);
    }
  }

  /**
   * This function will set tup the code playground
   * using CodeMirror.
   * If an initial content is passed in, that content will
   * be placed inside the editor and displayed to the user
   * as the editor first loaded.
   * 
   * After that, the editor will execute this code and
   * display the result to the result window on the right.
   * @param value       A javascript string contains the initial content
   *                    to be put inside the editor.
   */
  private InitiateEditor(value) {
    let s = document.createElement('script');
    s.textContent = `
      let editor = CodeMirror(document.getElementById('code-editor'), {
        value: ${value},
        mode: "text/html",
        theme: "night",
        lineNumbers: true,
        extraKeys: { "Ctrl-Space": "autocomplete" },
        autoCloseBrackets: true
      });
      editor.setSize("height", "100%");

      let run = function () {
          let content = editor.getValue();
          let iframe = document.getElementById('output');
          iframe = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
          iframe.document.open();
          iframe.document.write(content);
          iframe.document.close();
          return false;
      }

      run();
    `;
    document.body.appendChild(s);
  }

}
