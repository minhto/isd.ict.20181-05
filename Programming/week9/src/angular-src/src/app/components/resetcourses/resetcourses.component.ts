import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-resetcourses',
  templateUrl: './resetcourses.component.html',
  styleUrls: ['./resetcourses.component.css']
})
export class ResetcoursesComponent implements OnInit {
  /**
   * Keeping a reference to user progress.
   */
  private m_uUserProgress: UserProgress;
  /**
   * Contains the list of all courses.
   */
  private m_acCourseData: [Course];
  /**
   * Keeping a reference to the currently opened course.
   */
  private m_cCurrentCourse: Course;

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService,
    /**
     * This object is used to send request to course apis.
     */
    private courseService: CourseService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * The userProgress and courseData are retrieved from
   * userService using GetUserProgress() and RequestCourses()
   * functions.
   */
  ngOnInit() {
    this.userService.GetUserProgress().then(userProgress => {
      this.m_uUserProgress = <UserProgress>userProgress;
      this.courseService.RequestCourses().toPromise().then(courseData => {
        this.m_acCourseData = courseData;
      })
    })
  }

  /**
   * This function is used to open the modal window
   * that ask the user to verify if they really
   * want to reset this course or not.
   * @param course      The course that are going to be reset.
   */
  private OpenModal(course: Course) {
    this.m_cCurrentCourse = course;
    document.getElementById('myModal').style.display = "block";
  }

  /**
   * This function is used to close the modal window
   * that ask the user to verify if they really
   * want to reset this course or not.
   */
  private CloseModal() {
    document.getElementById('myModal').style.display = "none";
  }

  /**
   * This function is called when the user choose a course from 
   * the list of all courses, or from inside the OpenTakenCourse() function.
   * 
   * Set the current course inside courseService to the course that
   * are going to be opened.
   * Change the current route to 'learn'.
   * @param course    A course object indicates the course that are about to be opened.
   */
  private OpenCourse(course: Course) {
    this.courseService.SetCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  /**
   * This function is called when the user choose a course
   * from the list of taken course.
   * 
   * We take the reference of the taken course inside the list of all courses
   * and pass it to the OpenCourse() function.
   * @param takenCourse   Indicate the taken course that are about to be opened.
   */
  private OpenTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (takenCourse.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        this.OpenCourse(this.m_acCourseData[i]);
        break;
      }
    }
  }

  /**
   * This function is used to display the current module name
   * of each taken course.
   * @param course  A CourseProgress object that contains the current module for a taken course.
   */
  private GetModuleName(course: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (course.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        for (let j = 0; j < this.m_acCourseData[i].m_acModules.length; j++) {
          if (course.m_sProgress == this.m_acCourseData[i].m_acModules[j].m_sUrl) {
            return this.m_acCourseData[i].m_acModules[j].m_sName;
          }
        }
        break;
      }
    }
  }

  /**
   * After the modal window are displayed, if the user confirm
   * that they want to reset the course, this function is called.
   * 
   * The value of m_uUserProgress will be changed accordingly and
   * passed to the UpdateUserProgress() function inside userService.
   * 
   * After that, a result message from server will be displayed
   * and the application route redirect to 'home'.
   */
  private ResetTakenCourse() {
    if (!this.m_cCurrentCourse) return;
    for (let i = 0; i < this.m_uUserProgress.m_apProgresses.length; i++) {
      if (this.m_cCurrentCourse.m_sCourseUrl == this.m_uUserProgress.m_apProgresses[i].m_sCourseUrl) {
        this.m_uUserProgress.m_apProgresses.splice(i, 1);
        this.userService.UpdateUserProgress(this.m_uUserProgress).toPromise().then(() => {
          this.flashMessage.show("Course has been reset.", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['home']);
        })
        break;
      }
    }
  }
}
