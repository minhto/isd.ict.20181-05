import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  /**
   * A string contains value of the Old Password field.
   */
  private sOldPassword: String;
  /**
   * A string contains value of the New Password field.
   */
  private sNewPassword: String;
  /**
   * A string contains value of the Confirm Password field.
   */
  private sConfirmPassword: String;

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   */
  ngOnInit() {
  }

  /**
   * Invoked when the user click on the Submit button.
   * First, the username is retrieved from the userService object.
   * Then the ChangePassword() function from userService is called.
   *
   * After the request is send and a reply message from the server is received,
   * if the process is success, logout and display a success message,
   * otherwise display the error message from server.
   */
  private OnChangeSubmit() {
    if (this.sNewPassword != this.sConfirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      this.userService.GetUsername().then(username => {
        this.userService.ChangePassword({username: username, oldPassword: this.sOldPassword, newPassword: this.sNewPassword}).toPromise().then(data => {
          if (data.success) {
            this.sOldPassword = null;
            this.sNewPassword = null;
            this.sConfirmPassword = null;
            this.flashMessage.show("Success fully changed password. Logging out.", { cssClass: 'alert-success', timeout: 2000 });
            this.userService.Logout();
            this.router.navigate(['/login']);
          } else {
            this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
          }
        })
      })
    }
  }
}
