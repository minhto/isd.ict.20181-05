import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  /**
   * A reference to user progress.
   */
  private m_uUserProgress: UserProgress;
  /**
   * A string contains username.
   */
  private m_sUsername: String;
  /**
   * A boolean variable to indicate if this user has admin right or not.
   */
  private m_isAdmin: Boolean;
  /**
   * Contains the list of all courses.
   */
  private m_acCourseData: [Course];

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService,
    /**
     * This object is used to send request to course apis.
     */
    private courseService: CourseService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Get the username and admin priviledge indicator from userService
   * and save them.
   * 
   * After that, if the user is not an admin, get their progress and the
   * list of all courses.
   */
  ngOnInit() {
    this.userService.GetUsername().then(username => {
      this.m_sUsername = <String>username;
      this.m_isAdmin = false;
      if (this.userService.IsAdmin()) {
        this.m_isAdmin = true;
      } else {
        this.userService.GetUserProgress().then(userProgress => {
          this.m_uUserProgress = <UserProgress>userProgress;
          this.courseService.RequestCourses().toPromise().then(courseData => {
            this.m_acCourseData = courseData;
          })
        })
      }
    })
  }

  /**
   * This function is called when the user choose a course from 
   * the list of all courses, or from inside the OpenTakenCourse() function.
   * 
   * Set the current course inside courseService to the course that
   * are going to be opened.
   * Change the current route to 'learn'.
   * @param course    A course object indicates the course that are about to be opened.
   */
  private OpenCourse(course: Course) {
    this.courseService.SetCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  /**
   * This function is called when the user choose a course
   * from the list of taken course.
   * 
   * We take the reference of the taken course inside the list of all courses
   * and pass it to the OpenCourse() function.
   * @param takenCourse   Indicate the taken course that are about to be opened.
   */
  private OpenTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (takenCourse.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        this.OpenCourse(this.m_acCourseData[i]);
        break;
      }
    }
  }

  /**
   * This function is used to display the current module name
   * of each taken course.
   * @param course  A CourseProgress object that contains the current module for a taken course.
   */
  private GetModuleName(course: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (course.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        for (let j = 0; j < this.m_acCourseData[i].m_acModules.length; j++) {
          if (course.m_sProgress == this.m_acCourseData[i].m_acModules[j].m_sUrl) {
            return this.m_acCourseData[i].m_acModules[j].m_sName;
          }
        }
        break;
      }
    }
  }
}
