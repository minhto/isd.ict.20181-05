import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  /**
   * A string contains the email that the user used
   * to registered their account.
   */
  private m_sEmail: String;

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
    ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Create an User type object.
   */
  ngOnInit() {
  }

  /**
   * Invoked when the user click on the Submit button.
   * The content of the email field will be checked using the ValidateEmail() function.
   * 
   * If the email is legitimate, then its value
   * will be passed to the ResetUserPassword() function of userService.
   * 
   * Finally, a result message from server will be displayed to
   * show the result of the registration process.
   */
  private OnResetSubmit() {
    if (!this.ValidateEmail(this.m_sEmail)) {
      this.flashMessage.show("Invalid email address", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      const user = {
        email: this.m_sEmail
      }

      this.userService.ResetUserPassword(user).toPromise().then(data => {
        if (data.success) {
          this.flashMessage.show(data.msg, { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['login']);
        } else {
          this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        }
      })
    }
  }

  /**
   * Using a regular expression to check if a given string
   * is a legitimate email address or not.
   * @param email     A string that need to be tested.
   * @return          Return a boolean result to indicates
   *                  if the string is a valid email.
   */
  private ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
