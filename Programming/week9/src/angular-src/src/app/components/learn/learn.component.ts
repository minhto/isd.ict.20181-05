import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.css']
})
export class LearnComponent implements OnInit {
  /**
   * A reference to user progress.
   */
  private m_uUserProgress: UserProgress;
  /**
   * A reference to the currently opened course.
   */
  private m_cCurrentCourse: Course;
  /**
   * A int number indicates the index of the currently opened module.
   */
  private m_iCurrentModuleIndex: number;
  /**
   * A int number indicates the index of the latest module
   * that the user has learnt.
   */
  private m_iMaxModuleIndex: number;
  /**
   * A int number indicates the index of the latest module
   * that the user has learnt.
   */
  private m_iProgressIndex: number;
  /**
   * The html source of the main element inside the 'learn' view.
   */
  private m_content: any;

  constructor(
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService,
    /**
     * This object is used to send request to course apis.
     */
    private courseService: CourseService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Get the user progress from userService and current course from courseService.
   * After that, open the correspond module and display it to the 'learn' view.
   */
  ngOnInit() {
    this.userService.GetUserProgress().then(userProgress => {
      this.m_uUserProgress = <UserProgress>userProgress;
      this.courseService.GetCurrentCourse().then(currentCourse => {
        this.m_cCurrentCourse = <Course>currentCourse;

        this.m_iCurrentModuleIndex = -1;
        this.m_iMaxModuleIndex = -1;
        for (let i = 0; i < this.m_uUserProgress.m_apProgresses.length; i++) {
          if (this.m_cCurrentCourse.m_sCourseUrl == this.m_uUserProgress.m_apProgresses[i].m_sCourseUrl) {
            this.m_iProgressIndex = i;
            for (let j = 0; j < this.m_cCurrentCourse.m_acModules.length; j++) {
              if (this.m_uUserProgress.m_apProgresses[i].m_sProgress == this.m_cCurrentCourse.m_acModules[j].m_sUrl) {
                this.GetModule(j);
                break;
              } 
            }
            break;
          }
        }

        if (this.m_iCurrentModuleIndex == -1) {
          this.m_uUserProgress.m_apProgresses.push({
            m_sCourseUrl: this.m_cCurrentCourse.m_sCourseUrl,
            m_sProgress: this.m_cCurrentCourse.m_acModules[0].m_sUrl
          })
          this.m_iProgressIndex = this.m_uUserProgress.m_apProgresses.length - 1;
          this.GetModule(0);
        }
      })
    })
  }

  /**
   * Request the static html content for the corresponding course module
   * from the serve.
   * @param moduleIndex   An int number indicate the index of the module inside module list.
   */
  private GetModule(moduleIndex: number) {
    if (this.m_iMaxModuleIndex < moduleIndex) {
      this.m_iMaxModuleIndex = moduleIndex;

      this.m_uUserProgress.m_apProgresses[this.m_iProgressIndex].m_sProgress = this.m_cCurrentCourse.m_acModules[moduleIndex].m_sUrl;
      this.UpdateUserProgress().toPromise().then(() => {});
    }
    this.m_iCurrentModuleIndex = moduleIndex;
    this.courseService.GetModule(`${this.m_cCurrentCourse.m_sCourseUrl}/${this.m_cCurrentCourse.m_acModules[moduleIndex].m_sUrl}`).toPromise().then(content => {
      this.m_content = content.text();
    })
    this.CloseSidebar();
  }

  /**
   * This function invoke the UpdateUserProgress function of userService.
   * @return          Return the return result of the UpdateUserProgress function of userService.
   */
  private UpdateUserProgress() {
    return this.userService.UpdateUserProgress(this.m_uUserProgress);
  }
  
  /**
   * This function is used to open the side navigation bar.
   */
  private OpenSidebar() {
    document.getElementById("mySidebar").style.display = "block";
  }

  /**
   * This function is used to close the side navigation bar.
   */
  private CloseSidebar() {
    document.getElementById("mySidebar").style.display = "none";
  }

  /**
   * This function is used to open the previous course module.
   */
  private OpenPrevModule() {
    this.GetModule(this.m_iCurrentModuleIndex - 1);
    window.scrollTo(0, 0);
  }

  /**
   * This function is used to open the next course module.
   */
  private OpenNextModule() {
    this.GetModule(this.m_iCurrentModuleIndex + 1);
    window.scrollTo(0, 0);
  }
}
