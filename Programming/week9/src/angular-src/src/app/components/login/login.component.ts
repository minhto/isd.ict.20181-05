import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /**
   * Contains the value of username and password fields.
   */
  private m_uUser: User;

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Create an User type object.
   */
  ngOnInit() {
    this.m_uUser = new User();
  }

  /**
   * Invoked when the user click on the Submit button.
   * The m_uUser object contains username and password
   * will be passed to the AuthenticateUser() function of userService.
   * 
   * If the provided informations are not correct, the error message
   * from server will be displayed.
   * 
   * If the user is authenticated, proceed to verify if the user
   * has admin right, if not, store the userProgress and userName
   * in userService, navigate to the 'home' route.
   */
  private OnLoginSubmit() {
    this.userService.AuthenticateUser(this.m_uUser).toPromise().then(data => {
      if (data.success) {
        if (!data.admin) {
          this.userService.StoreUserProgress(data.progress);
        } else {
          this.userService.StoreUserPriviledge(data.admin);
        }
        this.userService.StoreUsername(this.m_uUser.m_sUsername);
        this.flashMessage.show("Logged in", { cssClass: 'alert-success', timeout: 2000 });
        this.router.navigate(['home']);
      } else {
        this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        this.router.navigate(['login']);
      }
    })
  }
}
