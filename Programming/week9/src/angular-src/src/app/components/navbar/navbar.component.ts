import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   */
  ngOnInit() {
  }

  /**
   * Invoked when the user click on the Logout button.
   * 
   * The function will call the Logout() function of userService,
   * which clear all saved informations.
   * After that, a pop up message is displayed and the application
   * route is changed to 'login'.
   */
  private OnLogoutClick() {
    this.userService.Logout();
    this.flashMessage.show("Logged out", {cssClass: 'alert-success', timeout: 2000});
    this.router.navigate(['/login']);
    return false;
  }
}
