import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  /**
   * Contains the value of username, password and email fields.
   */
  private m_uUser: User;
  /**
   * Contains the value of ConfirmPassword field in order to
   * compare with the Password field before registration.
   */
  private m_sConfirmPassword: String;

  constructor(
    /**
     * This object is used to redirect the application route.
     */
    private router: Router,
    /**
     * This object is used to display notification message.
     */
    private flashMessage: FlashMessagesService,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Create an User type object.
   */
  ngOnInit() {
    this.m_uUser = new User();
  }

  /**
   * Invoked when the user click on the Submit button.
   * First, the value of the m_sPassword field of object m_uUser
   * will be compared with the value of m_sConfirmPassword field.
   * 
   * If they do not match, an error message will pop up and prompt the
   * user to re-enter these values.
   * 
   * If they are the same, then the content of the email field
   * will be checked using the ValidateEmail() function.
   * 
   * If the email is legitimate, the m_uUser object contains username, password and email
   * will be passed to the RegisterUser() function of userService.
   * 
   * Finally, a result message from server will be displayed to
   * show the result of the registration process.
   */
  private OnRegisterSubmit() {
    if (this.m_uUser.m_sPassword != this.m_sConfirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
    } else if (!this.ValidateEmail(this.m_uUser.m_sEmail)) {
      this.flashMessage.show("Invalid email address", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      this.userService.RegisterUser(this.m_uUser).toPromise().then(data => {
        if (data.success) {
          this.flashMessage.show("Registered success", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['login']);
        } else {
          this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        }
      })
    }
  }

  /**
   * Using a regular expression to check if a given string
   * is a legitimate email address or not.
   * @param email     A string that need to be tested.
   * @return          Return a boolean result to indicates
   *                  if the string is a valid email.
   */
  private ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}