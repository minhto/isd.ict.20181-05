import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  /**
   * Contains the value of username.
   */
  private m_sUsername: String;

  constructor(
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * From OnInit interface.
   * This method is called first when the route is loaded.
   * 
   * Get the value of username from userService
   * and display it to the screen.
   */
  ngOnInit() {
    this.userService.GetUsername().then(username => {
      this.m_sUsername = <String>username;
    })
  }

}
