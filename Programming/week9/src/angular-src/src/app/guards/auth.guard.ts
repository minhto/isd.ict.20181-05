import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        /**
         * This object is used to send request to user apis.
         */
        private userService: UserService,
        /**
         * This object is used to redirect the application route.
         */
        private router: Router
    ) {}

    /**
     * From CanActivate interface.
     * @returns       Return a boolean result to indicate if the route
     *                protected by this guard can be accessed or not.
     */
    canActivate() {
        if (this.userService.IsLoggedIn()) return true;
        else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}