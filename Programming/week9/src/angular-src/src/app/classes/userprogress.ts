export class UserProgress {
    public m_asBadges: [String];
    public m_iExp: Number;
    public m_apProgresses: [CourseProgress];
    public m_sUserID: String;
}

export class CourseProgress {
    public m_sCourseUrl: String;
    public m_sProgress: String;
}