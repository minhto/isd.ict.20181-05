import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from '../classes/user';
import { UserProgress } from '../classes/userprogress';

@Injectable()
export class UserService {
  /**
   * Keep a reference to user progress object.
   */
  private m_uUserProgress: UserProgress;
  /**
   * Keep a reference to username.
   */
  private m_sUsername: String;

  constructor(
    /**
     * Imported from @angular/http
     * This object is used to send http requests.
     */
    private http: Http
  ) { }

  /**
   * Send a post request to api: /users/authenticate
   * @param user    An User object, contains username and password for authentication.
   * @returns       Return an observable object,
   *                return additional informations if the user is authenticated,
   *                return a reject message otherwise.
   */
  public AuthenticateUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
      .map(res => res.json());
  }

  /**
   * Send a post request to api: /users/register
   * @param user    An User object, contains username, password and email for registration.
   * @returns       Return an observable object to indicate the registration result.
   */
  public RegisterUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
      .map(res => res.json());
  }

  /**
   * Send a post request to api: /users/resetPassword
   * A notification email will be sent to the user's email.
   * @param user    A string contains user email.
   * @returns       Return an observable object contains the message from the server.
   */
  public ResetUserPassword(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/resetPassword', user, { headers: headers })
      .map(res => res.json());
  }

  /**
   * Save user progress to the m_uUserProgress variable
   * and to 'userProgress' item in localStorage.
   * @param userProgress  An UserProgress object.
   */
  public StoreUserProgress(userProgress: UserProgress) {
    this.m_uUserProgress = userProgress;
    localStorage.setItem('userProgress', JSON.stringify(userProgress));
  }

  /**
   * Save username to the m_sUsername variable
   * and to 'username' item in localStorage.
   * @param username    A string contains username.
   */
  public StoreUsername(username: String) {
    this.m_sUsername = username;
    localStorage.setItem('username', JSON.stringify(username));
  }

  /**
   * Save user priviledge indicator to the isAdmin variable.
   * @param isAdmin     A boolean variale indicate whether this user
   *                    is an admin or not.
   */
  public StoreUserPriviledge(isAdmin: Boolean) {
    localStorage.setItem('isAdmin', JSON.stringify(isAdmin));
  }

  /**
   * Get the current user progress by
   * returning the value of m_uUserProgress variable
   * or the content of 'userProgress' item in localStorage.
   * 
   * Reject the call to this function if the value of
   * user progress can not be found. Log out of the application after that.
   * @return            Return the user's current progress.
   */
  public GetUserProgress() {
    return new Promise((resolve, reject) => {
      if (this.m_uUserProgress) {
        resolve(this.m_uUserProgress);
      } else {
        let savedUserProgress = localStorage.getItem('userProgress');
        if (savedUserProgress !== null) {
          this.m_uUserProgress = JSON.parse(savedUserProgress);
          resolve(JSON.parse(savedUserProgress));
        } else {
          this.Logout();
          reject();
        }
      }
    })
  }

  /**
   * Get the username by
   * returning the value of m_sUsername variable
   * or the content of 'username' item in localStorage.
   * 
   * Reject the call to this function if the value of
   * username can not be found. Log out of the application after that.
   * @return            Return a string contains username.
   */
  public GetUsername() {
    return new Promise((resolve, reject) => {
      if (this.m_sUsername) {
        resolve(this.m_sUsername);
      } else {
        let savedUsername = localStorage.getItem('username');
        if (savedUsername !== null) {
          this.m_sUsername = JSON.parse(savedUsername);
          resolve(JSON.parse(savedUsername));
        } else {
          this.Logout();
          reject();
        }
      }
    })
  }

  /**
   * Confirm whether or not the current user has admin right
   * by returning the content of 'isAdmin' item in localStorage.
   * 
   * @return            Return a boolean admin-priviledge-indicator.
   */
  public IsAdmin(): Boolean {
    return !(localStorage.getItem('isAdmin') == null);
  }

  /**
   * Sent a put request to api: /userProgress/update
   * 
   * @param userProgress  An UserProgress object contains the new updated progress.
   * @return            Return an observable object contains the reply messages from server.
   */
  public UpdateUserProgress(userProgress: UserProgress) {
    this.StoreUserProgress(userProgress);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/userProgress/update', {userID: userProgress.m_sUserID, progresses: userProgress.m_apProgresses}, { headers: headers })
      .map(res => res.json());
  }

  /**
   * Sent a put request to api: /users/changePassword
   * 
   * @param user        Contains username, oldPassword for authentication and newPassword.
   * @return            Return an observable object contains the reply messages from server.
   */
  public ChangePassword(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/users/changePassword', user, { headers: headers })
      .map(res => res.json());
  }

  /**
   * Confirm the state of the application: whether the user has logged in or not.
   * 
   * @return            Return a boolean result indicate if the user has logged in.
   */
  public IsLoggedIn(): Boolean {
    return !(localStorage.getItem('username') == null);
  }

  /**
   * Clear all saved data.
   */
  public Logout() {
    localStorage.clear();
    this.m_uUserProgress = null;
    this.m_sUsername = null;
  }
}
