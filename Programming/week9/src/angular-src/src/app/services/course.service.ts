import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { UserService } from './user.service';
import 'rxjs/add/operator/map';

import { Course } from '../classes/course';

@Injectable()
export class CourseService {
  /**
   * Keep a reference to the list of all courses.
   */
  private m_acCourses: [Course];
  /**
   * Keep a reference to the course that is being opened.
   */
  private m_cCurrentCourse: Course;

  constructor(
    /**
     * Imported from @angular/http
     * This object is used to send http requests.
     */
    private http: Http,
    /**
     * This object is used to send request to user apis.
     */
    private userService: UserService
  ) { }

  /**
   * Send a get request to api: /course/list
   * @returns       Return an observable object, contains the list of all courses.
   */
  public RequestCourses() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/courses/list', { headers: headers })
      .map(res => res.json());
  }

  /**
   * Get the list of all courses.
   * If the course list has been requested before from the server, return it,
   * otherwise request it by invoke the RequestCourses() function.
   * @returns       Return a promise contains the list of all courses.
   */
  public GetCourses() {
    return new Promise((resolve, reject) => {
      if (this.m_acCourses) resolve(this.m_acCourses);
      else {
        this.RequestCourses().toPromise().then(data => {
          this.m_acCourses = data;
          resolve(data);
        })
      }
    })
  }

  /**
   * Set the value of variable m_cCurrentCourse and 'currentCourse' item
   * in localStorage to keep track of the currently opening course.
   * @param course  The course that is being opened.
   */
  public SetCurrentCourse(course: Course) {
    this.m_cCurrentCourse = course;
    localStorage.setItem('currentCourse', JSON.stringify(course));
  }

  /**
   * Get the current course by either extracting it from the m_cCurrentCourse
   * variable or from the 'currentCourse' item in the local storage.
   * 
   * If the current course can not be found, reject the call to this function
   * and log out of the application.
   * @returns       Return a promise contains the current course.
   */
  public GetCurrentCourse() {
    return new Promise((resolve, reject) => {
      if (this.m_cCurrentCourse) resolve(this.m_cCurrentCourse);
      else {
        let savedCurrentCourse = localStorage.getItem('currentCourse');
        if (savedCurrentCourse !== null) {
          this.m_cCurrentCourse = JSON.parse(savedCurrentCourse);
          resolve(JSON.parse(savedCurrentCourse));
        } else {
          this.m_acCourses = null;
          this.m_cCurrentCourse = null;
          this.userService.Logout();
          reject();
        }
      }
    })
  }

  /**
   * Send a request to api at: /modules/images/{course}.png
   * to retrive the image of that course.
   * @param course  A string indicate the course name.
   * @returns       Return an observable contains the requested image.
   */
  public RequestCourseImage(course: String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/images/${course}.png`, { headers: headers });
  }

  /**
   * Send a request to api at: /modules/{url}
   * to retrive the content of a module of the course.
   * @param url     A string indicate the course url.
   * @returns       Return an observable contains the module content.
   */
  public GetModule(url: String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/${url}`, { headers: headers });
  }
}
