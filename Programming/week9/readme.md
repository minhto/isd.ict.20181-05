# i. Description of the homework/project

1. Class Design 
* Operation Design 
* Method design 

2. Data modeling 
* Design, implement indexes for database. Write description(which type of indexes you use, how to implement,..)
* Use external schema (view, stored procedure) for connecting and manipulating database. 

3. Programming 
* Propose coding convention 
* Programming following that convention 
* Tune/Refactor your source code 
* Using tools to create document for your program, including classes, attributes and methods. 

# ii. Assignment of each member. 
*Minh*:
 
1. Design, implement indexes 
2. Choose DBMS: SQLite 

*Tuan*: 
1. Propose coding convention.
2. Refractor/tuning code base according to the new coding convention.
3. Choose documentation module: [Typedoc](https://typedoc.org/).
4. Document and comment code base according [Typedoc](https://typedoc.org/) documentation.

*Cem*:
1. Refractor/tuning code base according to the new coding convention.
2. Document and comment code base according [Typedoc](https://typedoc.org/) documentation.