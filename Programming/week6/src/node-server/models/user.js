const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    username: String,
    password: String,
    admin: Boolean
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getAllUsers = (callback) => {
    User.find({}, callback);
}

module.exports.addUser = (newUser, callback) => {
    newUser.save(callback);
}

module.exports.authenticateUser = (username, password, callback) => {
    User.findOne({username: username, password: password}, callback);
}

module.exports.getUserWithUsername = (username, callback) => {
    User.findOne({username: username}, callback);
}

module.exports.changePassword = (username, newPassword, callback) => {
    User.update({username: username}, {password: newPassword}, callback);
}