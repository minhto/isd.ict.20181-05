const express = require("express");
const router = express.Router();
const Course = require("../models/course");

router.get('/list', (req, res, next) => {
    Course.getAllCourses((err, courses) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(courses);
    })
})

router.post('/register', (req, res, next) => {
    let newCourse = new Course({
        courseName: req.body.courseName,
        courseUrl: req.body.courseUrl,
        modules: req.body.modules
    });

    Course.addCourse(newCourse, (err) => {
        if (err)
            res.json({
                success: false,
                msg: `Error occurred: ${err}`
            });
        else
            res.json({
                success: true,
                msg: "Course resgitered successful"
            });
    })
})

router.get('/:courseName', (req, res, next) => {
    Course.getCourse(req.params.courseName, (err, course) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(course);
    })
})

router.put('/update', (req, res, next) => {
    Course.updateCourseModules(req.body.courseName, req.body.courseUrl, req.body.modules, (err) => {
        if (err) {
            res.json({ success: false, msg: "Failed to update course modules"})
        } else {
            res.json({ success: true, msg: "Successfully update course modules" })
        }
    })
})

module.exports = router;