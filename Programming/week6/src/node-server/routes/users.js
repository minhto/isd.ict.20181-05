const express = require("express");
const router = express.Router();
const User = require("../models/user");
const UserProgress = require("../models/userProgress");

router.get('/list', (req, res, next) => {
    User.getAllUsers((err, users) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(users);
    })
})

router.post('/register', (req, res, next) => {
    let newUser = new User({
        username: req.body.username,
        password: req.body.password,
        admin: false
    });

    User.getUserWithUsername(newUser.username, (err, user) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        } else {
            if (!user) {
                User.addUser(newUser, (err, user) => {
                    if (err)
                        res.json({
                            success: false,
                            msg: `Error occurred: ${err}`
                        });
                    else
                        res.json({
                            success: true,
                            msg: "User resgitered successful"
                        });
                })
            } else {
                res.json({
                    success: false,
                    msg: `User already exists.`
                })
            }
        }
    })
})

router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    User.authenticateUser(username, password, (err, user) => {
        if (err) res.send(`Error occurred: ${err}`);
        if (!user)
            res.json({
                success: false,
                msg: "Wrong username or password"
            })
        else {
            if (!user.admin) {
                UserProgress.getUserProgressByUserID(user._id, (err, progress) => {
                    if (err) res.send(`Error occurred: ${err}`);
                    if (!progress) {
                        let newUserProgress = new UserProgress({
                            userID: user._id,
                            exp: 0,
                            badges: [],
                            progresses: []
                        })
                        UserProgress.newUserProgress(newUserProgress, (err) => {
                            if (err)
                            res.json({
                                success: false,
                                msg: `Error occurred: ${err}`
                            });
                            else
                            res.json({
                                success: true,
                                admin: user.admin,
                                progress: newUserProgress
                            })
                        })
                    } else
                    res.json({
                        success: true,
                        admin: user.admin,
                        progress: progress
                    })
                })
            } else {
                res.json({
                    success: true,
                    admin: user.admin
                })
            }
        }
    })
})

router.put('/changePassword', (req, res, next) => {
    User.authenticateUser(req.body.username, req.body.oldPassword, (err, user) => {
        if (err) res.send(`Error occured: ${err}`);
        if (!user) {
            res.json({ success: false, msg: "Wrong password"});
        } else {
            User.changePassword(req.body.username, req.body.newPassword, (err) => {
                if (err) {
                    res.json({ success: false, msg: "Failed to change user password"})
                } else {
                    res.json({ success: true, msg: "Successfully changed user password"});
                }
            })
        }
    }) 
})

module.exports = router;