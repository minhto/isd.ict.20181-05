import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { isDate } from 'util';

@Injectable()
export class UserService {
  userProgress: any;
  username: String;

  constructor(
    private http: Http
  ) { }

  authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
      .map(res => res.json());
  }

  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
      .map(res => res.json());
  }

  storeUserProgress(userProgress) {
    this.userProgress = userProgress;
    localStorage.setItem('userProgress', JSON.stringify(userProgress));
  }

  storeUsername(username) {
    this.username = username;
    localStorage.setItem('username', JSON.stringify(username));
  }

  storeUserPriviledge(isAdmin) {
    localStorage.setItem('isAdmin', JSON.stringify(isAdmin));
  }

  getUserProgress() {
    return new Promise((resolve, reject) => {
      if (this.userProgress) {
        resolve(this.userProgress);
      } else {
        let savedUserProgress = localStorage.getItem('userProgress');
        if (savedUserProgress !== null) {
          this.userProgress = JSON.parse(savedUserProgress);
          resolve(JSON.parse(savedUserProgress));
        } else {
          this.logout();
          reject();
        }
      }
    })
  }

  getUsername() {
    return new Promise((resolve, reject) => {
      if (this.username) {
        resolve(this.username);
      } else {
        let savedUsername = localStorage.getItem('username');
        if (savedUsername !== null) {
          this.username = JSON.parse(savedUsername);
          resolve(JSON.parse(savedUsername));
        } else {
          this.logout();
          reject();
        }
      }
    })
  }

  isAdmin() {
    return !(localStorage.getItem('isAdmin') == null);
  }

  updateUserProgress(userProgress) {
    this.storeUserProgress(userProgress);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/userProgress/update', {userID: userProgress.userID, progresses: userProgress.progresses}, { headers: headers })
      .map(res => res.json());
  }

  changePassword(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/users/changePassword', user, { headers: headers })
      .map(res => res.json());
  }

  isLoggedIn() {
    return !(localStorage.getItem('username') == null);
  }

  logout() {
    localStorage.clear();
    this.userProgress = null;
    this.username = null;
  }
}
