import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { UserService } from './user.service';
import 'rxjs/add/operator/map';

@Injectable()
export class CourseService {
  courses: any;
  currentCourse: any;

  constructor(
    private http: Http,
    private userService: UserService
  ) { }

  requestCourses() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/courses/list', { headers: headers })
      .map(res => res.json());
  }

  getCourses() {
    return new Promise((resolve, reject) => {
      if (this.courses) resolve(this.courses);
      else {
        this.requestCourses().toPromise().then(data => {
          this.courses = data;
          resolve(data);
        })
      }
    })
  }

  setCurrentCourse(course) {
    this.currentCourse = course;
    localStorage.setItem('currentCourse', JSON.stringify(course));
  }

  getCurrentCourse() {
    return new Promise((resolve, reject) => {
      if (this.currentCourse) resolve(this.currentCourse);
      else {
        let savedCurrentCourse = localStorage.getItem('currentCourse');
        if (savedCurrentCourse !== null) {
          this.currentCourse = JSON.parse(savedCurrentCourse);
          resolve(JSON.parse(savedCurrentCourse));
        } else {
          this.courses = null;
          this.currentCourse = null;
          this.userService.logout();
          reject();
        }
      }
    })
  }

  requestCourseImage(course) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/images/${course}.png`, { headers: headers });
  }

  getModule(url) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/${url}`, { headers: headers });
  }
}
