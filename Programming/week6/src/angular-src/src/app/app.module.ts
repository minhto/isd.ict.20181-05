import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotfoundComponent } from './components/notfound/notfound.component';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { LearnComponent } from './components/learn/learn.component';
import { CoursemanagementComponent } from './components/coursemanagement/coursemanagement.component';

import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './guards/auth.guard';
import { NotAuthGuard } from './guards/notauth.guard';
import { AdminGuard } from './guards/admin.guard';
import { NotAdminGuard } from './guards/notadmin.guard';

import { UserService } from './services/user.service';
import { CourseService } from './services/course.service';

const appRoutes: Routes = [
  {path:'404', component: NotfoundComponent},
  {path:'home', component: HomeComponent, canActivate: [AuthGuard] },
  {path:'login', component: LoginComponent, canActivate: [NotAuthGuard] },
  {path:'register', component: RegisterComponent, canActivate: [NotAuthGuard] },
  {path:'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
  {path:'learn', component: LearnComponent, canActivate: [AuthGuard] },
  {path:'coursemanagement', component: CoursemanagementComponent, canActivate: [AdminGuard] },
  {path: '**', redirectTo: '404'}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ChangepasswordComponent,
    HomeComponent,
    NotfoundComponent,
    LearnComponent,
    CoursemanagementComponent,
    NavbarComponent,
    NotfoundComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ChangepasswordComponent,
    LearnComponent,
    CoursemanagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule
  ],
  providers: [
    AuthGuard,
    NotAuthGuard,
    AdminGuard,
    NotAdminGuard,
    UserService,
    CourseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
