import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username: String;
  password: String;
  confirmPassword: String;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService) { }

  ngOnInit() {
  }

  onRegisterSubmit() {
    if (this.password != this.confirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
      this.router.navigate(['register']);
    } else {
      const user = {
        username: this.username,
        password: this.password
      }
      
      this.userService.registerUser(user).toPromise().then(data => {
        if (data.success) {
          this.flashMessage.show("Registered success", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['login']);
        } else {
          this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
          this.router.navigate(['register']);
        }
      })
    }
  }

}