import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userProgress: any;
  username: any;
  isAdmin: any;
  courseData: any;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.getUsername().then(username => {
      this.username = username;
      this.isAdmin = false;
      if (this.userService.isAdmin()) {
        this.isAdmin = true;
      } else {
        this.userService.getUserProgress().then(userProgress => {
          this.userProgress = userProgress;
          this.courseService.requestCourses().toPromise().then(courseData => {
            this.courseData = courseData;
          })
        })
      }
    })
  }

  openCourse(course) {
    this.courseService.setCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  openTakenCourse(takenCourse) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (takenCourse.courseUrl == this.courseData[i].courseUrl) {
        this.openCourse(this.courseData[i]);
        break;
      }
    }
  }

  getModuleName(course) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (course.courseUrl == this.courseData[i].courseUrl) {
        for (let j = 0; j < this.courseData[i].modules.length; j++) {
          if (course.progress == this.courseData[i].modules[j].url) {
            return this.courseData[i].modules[j].name;
          }
        }
        break;
      }
    }
  }

  resetTakenCourse(course) {
    for (let i = 0; i < this.userProgress.progresses.length; i++) {
      if (course.courseUrl == this.userProgress.progresses[i].courseUrl) {
        this.userProgress.progresses.splice(i, 1);
        this.userService.updateUserProgress(this.userProgress).toPromise().then(() => {
          this.flashMessage.show("Course has been resetted", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['home']);
        })
        break;
      }
    }
  }
}
