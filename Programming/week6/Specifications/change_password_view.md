# View mockup
![change_password_view_mockup](https://bitbucket.org/minhto/isd.ict.20181-05/raw/95967af7e5492111d596e5891edb65863584c432/Project/week6/mockups/change_password_view.png)
# Screen specifications
|Control|Action|Function|
|---|---|---|
|Logout button|Hover|Flash/change color to indicate that the button is being hovered|
|Home button|Click|Redirect application to the *Home* view|
|Logout button|Click|Clear local storage, redirect to *Login* view|
|Old password input field|Enter password|The user their current password so the system could verify that the one using the application is indeed the owner of this account|
|New password input field|Enter password|The user enter a new password using characters that are accepted by the application|
|Confirm password input field|Enter password|The user confirm their new password by re-enter it. The value of this field and the above one must be the same|
|Submit button|Click|The application confirms if there is a difference between *New password* and *Confirm password* view, then send a request view both *Old password* and *New password* to the back-end system|