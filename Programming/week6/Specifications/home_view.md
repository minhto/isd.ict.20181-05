# View mockup
![home_view_mockup](https://bitbucket.org/minhto/isd.ict.20181-05/raw/c2d19b5e881a062edeae934ea6ccb729bf66d874/Project/week6/mockups/home_view.png)
# Screen specifications
|Control|Action|Function|
|---|---|---|
|Home button|Hover|Flash/change color to indicate that the button is being hovered|
|Change Password button|Click|Redirect application to the *Change Password* view|
|Logout button|Click|Clear local storage, redirect to *Login* view|
|Reset Course button|Click|Pop up a window asking the user for reset confirmation|
|Confirm reset button|Click|Send the request to back-end system to delete all progress of user in this particular course|
|Course's Image|Click|Redirect to *Course* View|
