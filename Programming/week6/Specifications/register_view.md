# View mockup
![register_view_mockup](https://bitbucket.org/minhto/isd.ict.20181-05/raw/c2d19b5e881a062edeae934ea6ccb729bf66d874/Project/week6/mockups/register_view.png)
# Screen specifications
|Control|Action|Function|
|---|---|---|
|Register button|Hover|Flash/change color to indicate that the button is being hovered|
|Login button|Click|Redirect application to the *Login* view|
|Username input field|Enter Username|The User fills in the username he wants to use to register|
|Password input field|Enter password|The user enter password he wants to use to register using characters that are accepted by the application|
|Confirm password input field|Enter password|The user confirm his password by re-enter it. The value of this field and the above one must be the same|
|Submit button|Click|The application confirms if the Username is duplicated, the Password satisfies requirement then send a request view of *Username* and *Password* to the back-end system|