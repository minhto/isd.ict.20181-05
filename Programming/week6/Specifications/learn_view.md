# View mockup
![learn_view_mockup](https://bitbucket.org/minhto/isd.ict.20181-05/raw/717f57d26125bfea796080421a683f65fe8fbdf0/Project/week6/mockups/learn_view.png)
# Screen specifications
|Control|Action|Function|
|---|---|---|
|Logout button|Hover|Flash/change color to indicate that the button is being hovered|
|Home button|Click|Redirect application to the *Home* view|
|Logout button|Click|Clear local storage, redirect to *Login* view|
|x button on the sidebar|Click|Close the sidebar|
|Buttons in the sidebar (bright color)|Click|Change the main content of the *Learn* view to the corresponding content. These are the contents that the user has already learnt|
|Buttons in the sidebar (dark color)|Click|Nothing happen. These are the contents that the user has yet read|
|Previous button|Click|Change the main content of the *Learn* view to the previous tab|
|Next button|Click|Change the main content of the *Learn* view to the next tab|