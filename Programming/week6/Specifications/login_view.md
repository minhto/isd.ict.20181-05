# View mockup
![login_view_mockup](https://bitbucket.org/minhto/isd.ict.20181-05/raw/c2d19b5e881a062edeae934ea6ccb729bf66d874/Project/week6/mockups/login_view.png)
# Screen specifications
|Control|Action|Function|
|---|---|---|
|Register button|Click|Redirect application to the *Register* view|
|Username input field|Enter Username|The user provides username information for logging in|
|Password input field|Enter password|The user provides password information for logging in|
|Submit button|Click|The application sends a request of *Username* and *Password* to the back-end system|