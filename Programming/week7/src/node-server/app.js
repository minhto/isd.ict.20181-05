const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config/database");

mongoose.connect(config.database);

mongoose.connection.on('connected', () => {
    console.log(`Connected to database ${config.database}`);
})

mongoose.connection.on('error', (err) => {
    console.log(`Database error: ${err}`);
})

const app = express();
const port = 3000;

// Middle wares
app.use(cors());
app.use(bodyParser.json());

// Routing
app.use("/users", require("./routes/users"));
app.use("/userProgress", require("./routes/userProgress"));
app.use("/courses", require("./routes/courses"));

// Static html
app.use("/modules", express.static('public'));

// Angular dist
// app.use(express.static(path.join(__dirname, 'public/dist')));
// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public/dist/index.html'))
// })

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})