const express = require("express");
const router = express.Router();
const User = require("../models/user");
const UserProgress = require("../models/userProgress");

const nodemailer = require("nodemailer");
const mailConfig = require("../config/mail.json");

router.get('/list', (req, res, next) => {
    User.getAllUsers((err, users) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(users);
    })
})

router.post('/register', (req, res, next) => {
    let newUser = new User({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        admin: false
    });

    User.getUserWithUsername(newUser.username, (err, user) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        } else {
            if (!user) {
                User.addUser(newUser, (err, user) => {
                    if (err)
                        res.json({
                            success: false,
                            msg: `Error occurred: ${err}`
                        });
                    else
                        res.json({
                            success: true,
                            msg: "User resgitered successful"
                        });
                })
            } else {
                res.json({
                    success: false,
                    msg: `User already exists.`
                })
            }
        }
    })
})

router.post('/authenticate', (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    User.authenticateUser(username, password, (err, user) => {
        if (err) res.send(`Error occurred: ${err}`);
        if (!user)
            res.json({
                success: false,
                msg: "Wrong username or password"
            })
        else {
            if (!user.admin) {
                UserProgress.getUserProgressByUserID(user._id, (err, progress) => {
                    if (err) res.send(`Error occurred: ${err}`);
                    if (!progress) {
                        let newUserProgress = new UserProgress({
                            userID: user._id,
                            exp: 0,
                            badges: [],
                            progresses: []
                        })
                        UserProgress.newUserProgress(newUserProgress, (err) => {
                            if (err)
                                res.json({
                                    success: false,
                                    msg: `Error occurred: ${err}`
                                });
                            else
                                res.json({
                                    success: true,
                                    admin: user.admin,
                                    progress: newUserProgress
                                })
                        })
                    } else
                        res.json({
                            success: true,
                            admin: user.admin,
                            progress: progress
                        })
                })
            } else {
                res.json({
                    success: true,
                    admin: user.admin
                })
            }
        }
    })
})

router.put('/changePassword', (req, res, next) => {
    User.authenticateUser(req.body.username, req.body.oldPassword, (err, user) => {
        if (err) res.send(`Error occured: ${err}`);
        if (!user) {
            res.json({ success: false, msg: "Wrong password" });
        } else {
            User.changePassword(req.body.username, req.body.newPassword, (err) => {
                if (err) {
                    res.json({ success: false, msg: "Failed to change user password" })
                } else {
                    res.json({ success: true, msg: "Successfully changed user password" });
                }
            })
        }
    })
})

router.post('/resetPassword', (req, res, next) => {
    User.getUserWithEmail(req.body.email, (err, user) => {
        if (err) res.send(`Error occured: ${err}`);
        if (!user) {
            res.json({ success: false, msg: "There is no user with this email" });
        } else {
            User.changePassword(user.username, user.username, (err) => {
                if (err) {
                    res.json({ success: false, msg: "Failed to reset user password" });
                } else {
                    // Send email to user
                    let transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: mailConfig.user,
                            pass: mailConfig.pass
                        }
                    })

                    let mailOptions = {
                        from: mailConfig.user,
                        to: user.email,
                        subject: 'Resetting password',
                        text: 'Your password has been set to your username. Please change your password right away.'
                    }

                    transporter.sendMail(mailOptions, (err, info) => {
                        if (err) {
                            res.send(`Error occured: ${err}`)
                        } else {
                            res.json({
                                success: true,
                                msg: "Successfully reset user password. Please check your email."
                            })
                        }
                    })
                }
            })
        }
    })
})

module.exports = router;