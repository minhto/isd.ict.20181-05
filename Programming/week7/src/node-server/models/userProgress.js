const mongoose = require("mongoose");

const ProgressChema = mongoose.Schema({
    courseUrl: String,
    progress: String
})

const UserProgressSchema = mongoose.Schema({
    userID: String,
    exp: Number,
    badges: [String],
    progresses: [ProgressChema]
})

const UserProgress = module.exports = mongoose.model('UserProgress', UserProgressSchema);

module.exports.getAllUserProgresses = (callback) => {
    UserProgress.find({}, callback);
}

module.exports.newUserProgress = (progress, callback) => {
    progress.save(callback);
}

module.exports.getUserProgressByUserID = (id, callback) => {
    UserProgress.findOne({userID: id}, callback);
}

module.exports.updateUserProgress = (userID, progresses, callback) => {
    UserProgress.update({userID: userID}, {progresses: progresses}, callback);
}

module.exports.updateBadge = (userID, badges, callback) => {
    UserProgress.update({userID: userID}, {badges: badges}, callback);
}