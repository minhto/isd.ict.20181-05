const mongoose = require("mongoose");

const ModuleSchema = mongoose.Schema({
    name: String,
    url: String
})

const CourseSchema = mongoose.Schema({
    courseName: String,
    courseUrl: String,
    modules: [ModuleSchema]
})

const Course = module.exports = mongoose.model('Course', CourseSchema);

module.exports.getAllCourses = (callback) => {
    Course.find({}, callback);
}

module.exports.addCourse = (newCourse, callback) => {
    newCourse.save(callback);
}

module.exports.getCourse = (courseName, callback) => {
    Course.findOne({courseName: courseName}, callback);
}

module.exports.updateCourseModules = (courseName, courseUrl, modules, callback) => {
    Course.update({courseName: courseName, courseUrl: courseUrl}, {modules: modules}, callback);
}