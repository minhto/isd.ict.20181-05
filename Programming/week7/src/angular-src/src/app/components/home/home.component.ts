import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private userProgress: UserProgress;
  private username: String;
  private isAdmin: Boolean;
  private courseData: [Course];

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.getUsername().then(username => {
      this.username = <String>username;
      this.isAdmin = false;
      if (this.userService.isAdmin()) {
        this.isAdmin = true;
      } else {
        this.userService.getUserProgress().then(userProgress => {
          this.userProgress = <UserProgress>userProgress;
          this.courseService.requestCourses().toPromise().then(courseData => {
            this.courseData = courseData;
          })
        })
      }
    })
  }

  private openCourse(course: Course) {
    this.courseService.setCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  private openTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (takenCourse.courseUrl == this.courseData[i].courseUrl) {
        this.openCourse(this.courseData[i]);
        break;
      }
    }
  }

  private getModuleName(course: CourseProgress) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (course.courseUrl == this.courseData[i].courseUrl) {
        for (let j = 0; j < this.courseData[i].modules.length; j++) {
          if (course.progress == this.courseData[i].modules[j].url) {
            return this.courseData[i].modules[j].name;
          }
        }
        break;
      }
    }
  }
}
