import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-resetcourses',
  templateUrl: './resetcourses.component.html',
  styleUrls: ['./resetcourses.component.css']
})
export class ResetcoursesComponent implements OnInit {
  private userProgress: UserProgress;
  private courseData: [Course];
  private currentCourse: Course;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.getUserProgress().then(userProgress => {
      this.userProgress = <UserProgress>userProgress;
      this.courseService.requestCourses().toPromise().then(courseData => {
        this.courseData = courseData;
      })
    })
  }

  private openModal(course: Course) {
    this.currentCourse = course;
    document.getElementById('myModal').style.display = "block";
  }

  private closeModal() {
    document.getElementById('myModal').style.display = "none";
  }

  private openCourse(course: Course) {
    this.courseService.setCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  private openTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (takenCourse.courseUrl == this.courseData[i].courseUrl) {
        this.openCourse(this.courseData[i]);
        break;
      }
    }
  }

  private getModuleName(course: CourseProgress) {
    for (let i = 0; i < this.courseData.length; i++) {
      if (course.courseUrl == this.courseData[i].courseUrl) {
        for (let j = 0; j < this.courseData[i].modules.length; j++) {
          if (course.progress == this.courseData[i].modules[j].url) {
            return this.courseData[i].modules[j].name;
          }
        }
        break;
      }
    }
  }

  private resetTakenCourse() {
    if (!this.currentCourse) return;
    for (let i = 0; i < this.userProgress.progresses.length; i++) {
      if (this.currentCourse.courseUrl == this.userProgress.progresses[i].courseUrl) {
        this.userProgress.progresses.splice(i, 1);
        this.userService.updateUserProgress(this.userProgress).toPromise().then(() => {
          this.flashMessage.show("Course has been resetted", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['home']);
        })
        break;
      }
    }
  }
}
