import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-coursemanagement',
  templateUrl: './coursemanagement.component.html',
  styleUrls: ['./coursemanagement.component.css']
})
export class CoursemanagementComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.courseService.getCourses().then(courses => {
      console.log(courses);
    })
  }

}
