import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  private oldPassword: String;
  private newPassword: String;
  private confirmPassword: String;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  private onChangeSubmit() {
    if (this.newPassword != this.confirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      this.userService.getUsername().then(username => {
        this.userService.changePassword({username: username, oldPassword: this.oldPassword, newPassword: this.newPassword}).toPromise().then(data => {
          if (data.success) {
            this.oldPassword = null;
            this.newPassword = null;
            this.confirmPassword = null;
            this.flashMessage.show("Success fully changed password. Logging out.", { cssClass: 'alert-success', timeout: 2000 });
            this.userService.logout();
            this.router.navigate(['/login']);
          } else {
            this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
          }
        })
      })
    }
  }
}
