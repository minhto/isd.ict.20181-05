import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private user: User;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.user = new User();
  }

  private onLoginSubmit() {
    this.userService.authenticateUser(this.user).toPromise().then(data => {
      if (data.success) {
        if (!data.admin) {
          this.userService.storeUserProgress(data.progress);
        } else {
          this.userService.storeUserPriviledge(data.admin);
        }
        this.userService.storeUsername(this.user.username);
        this.flashMessage.show("Logged in", { cssClass: 'alert-success', timeout: 2000 });
        this.router.navigate(['home']);
      } else {
        this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        this.router.navigate(['login']);
      }
    })
  }
}
