import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  email: String;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
    ) { }

  ngOnInit() {
  }

  onResetSubmit() {
    if (!this.validateEmail(this.email)) {
      this.flashMessage.show("Invalid email address", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      const user = {
        email: this.email
      }

      this.userService.resetUserPassword(user).toPromise().then(data => {
        if (data.success) {
          this.flashMessage.show(data.msg, { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['login']);
        } else {
          this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        }
      })
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
