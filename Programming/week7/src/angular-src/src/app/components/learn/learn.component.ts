import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.css']
})
export class LearnComponent implements OnInit {
  private userProgress: UserProgress;
  private currentCourse: Course;
  private currentModuleIndex: number;
  private maxModuleIndex: number;
  private progressIndex: number;
  private content: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.getUserProgress().then(userProgress => {
      this.userProgress = <UserProgress>userProgress;
      this.courseService.getCurrentCourse().then(currentCourse => {
        this.currentCourse = <Course>currentCourse;

        this.currentModuleIndex = -1;
        this.maxModuleIndex = -1;
        for (let i = 0; i < this.userProgress.progresses.length; i++) {
          if (this.currentCourse.courseUrl == this.userProgress.progresses[i].courseUrl) {
            this.progressIndex = i;
            for (let j = 0; j < this.currentCourse.modules.length; j++) {
              if (this.userProgress.progresses[i].progress == this.currentCourse.modules[j].url) {
                this.getModule(j);
                break;
              } 
            }
            break;
          }
        }

        if (this.currentModuleIndex == -1) {
          this.userProgress.progresses.push({
            courseUrl: this.currentCourse.courseUrl,
            progress: this.currentCourse.modules[0].url
          })
          this.progressIndex = this.userProgress.progresses.length - 1;
          this.getModule(0);
        }
      })
    })
  }

  private getModule(moduleIndex: number) {
    if (this.maxModuleIndex < moduleIndex) {
      this.maxModuleIndex = moduleIndex;

      this.userProgress.progresses[this.progressIndex].progress = this.currentCourse.modules[moduleIndex].url;
      this.updateUserProgress().toPromise().then(() => {});
    }
    this.currentModuleIndex = moduleIndex;
    this.courseService.getModule(`${this.currentCourse.courseUrl}/${this.currentCourse.modules[moduleIndex].url}`).toPromise().then(content => {
      this.content = content.text();
    })
    this.closeSidebar();
  }

  private updateUserProgress() {
    return this.userService.updateUserProgress(this.userProgress);
  }

  // Toggle sidebar
  private openSidebar() {
    document.getElementById("mySidebar").style.display = "block";
  }

  private closeSidebar() {
    document.getElementById("mySidebar").style.display = "none";
  }

  // Modules navigation
  private openPrevModule() {
    this.getModule(this.currentModuleIndex - 1);
    window.scrollTo(0, 0);
  }

  private openNextModule() {
    this.getModule(this.currentModuleIndex + 1);
    window.scrollTo(0, 0);
  }
}
