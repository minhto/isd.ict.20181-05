import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { UserService } from './user.service';
import 'rxjs/add/operator/map';

import { Course } from '../classes/course';

@Injectable()
export class CourseService {
  private courses: [Course];
  private currentCourse: Course;

  constructor(
    private http: Http,
    private userService: UserService
  ) { }

  public requestCourses() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/courses/list', { headers: headers })
      .map(res => res.json());
  }

  public getCourses() {
    return new Promise((resolve, reject) => {
      if (this.courses) resolve(this.courses);
      else {
        this.requestCourses().toPromise().then(data => {
          this.courses = data;
          resolve(data);
        })
      }
    })
  }

  public setCurrentCourse(course: Course) {
    this.currentCourse = course;
    localStorage.setItem('currentCourse', JSON.stringify(course));
  }

  public getCurrentCourse() {
    return new Promise((resolve, reject) => {
      if (this.currentCourse) resolve(this.currentCourse);
      else {
        let savedCurrentCourse = localStorage.getItem('currentCourse');
        if (savedCurrentCourse !== null) {
          this.currentCourse = JSON.parse(savedCurrentCourse);
          resolve(JSON.parse(savedCurrentCourse));
        } else {
          this.courses = null;
          this.currentCourse = null;
          this.userService.logout();
          reject();
        }
      }
    })
  }

  public requestCourseImage(course: String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/images/${course}.png`, { headers: headers });
  }

  public getModule(url: String) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(`http://localhost:3000/modules/${url}`, { headers: headers });
  }
}
