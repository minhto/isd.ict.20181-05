import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from '../classes/user';
import { UserProgress } from '../classes/userprogress';

@Injectable()
export class UserService {
  private userProgress: UserProgress;
  private username: String;

  constructor(
    private http: Http
  ) { }

  public authenticateUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, { headers: headers })
      .map(res => res.json());
  }

  public registerUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register', user, { headers: headers })
      .map(res => res.json());
  }

  public resetUserPassword(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/resetPassword', user, { headers: headers })
      .map(res => res.json());
  }

  public storeUserProgress(userProgress: UserProgress) {
    this.userProgress = userProgress;
    localStorage.setItem('userProgress', JSON.stringify(userProgress));
  }

  public storeUsername(username: String) {
    this.username = username;
    localStorage.setItem('username', JSON.stringify(username));
  }

  public storeUserPriviledge(isAdmin: Boolean) {
    localStorage.setItem('isAdmin', JSON.stringify(isAdmin));
  }

  public getUserProgress() {
    return new Promise((resolve, reject) => {
      if (this.userProgress) {
        resolve(this.userProgress);
      } else {
        let savedUserProgress = localStorage.getItem('userProgress');
        if (savedUserProgress !== null) {
          this.userProgress = JSON.parse(savedUserProgress);
          resolve(JSON.parse(savedUserProgress));
        } else {
          this.logout();
          reject();
        }
      }
    })
  }

  public getUsername() {
    return new Promise((resolve, reject) => {
      if (this.username) {
        resolve(this.username);
      } else {
        let savedUsername = localStorage.getItem('username');
        if (savedUsername !== null) {
          this.username = JSON.parse(savedUsername);
          resolve(JSON.parse(savedUsername));
        } else {
          this.logout();
          reject();
        }
      }
    })
  }

  public isAdmin(): Boolean {
    return !(localStorage.getItem('isAdmin') == null);
  }

  public updateUserProgress(userProgress: UserProgress) {
    this.storeUserProgress(userProgress);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/userProgress/update', {userID: userProgress.userID, progresses: userProgress.progresses}, { headers: headers })
      .map(res => res.json());
  }

  public changePassword(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/users/changePassword', user, { headers: headers })
      .map(res => res.json());
  }

  public isLoggedIn(): Boolean {
    return !(localStorage.getItem('username') == null);
  }

  public logout() {
    localStorage.clear();
    this.userProgress = null;
    this.username = null;
  }
}
