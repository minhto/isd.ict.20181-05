import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class NotAdminGuard implements CanActivate {
    constructor(
        private userService: UserService,
        private router: Router
    ) {}

    canActivate() {
        if (this.userService.isLoggedIn()) {
            if (!this.userService.isAdmin()) return true;
            else {
                this.router.navigate(['/home']);
                return false;
            }
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}