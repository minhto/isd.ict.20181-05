export class UserProgress {
    public badges: [String];
    public exp: Number;
    public progresses: [CourseProgress];
    public userID: String;
}

export class CourseProgress {
    public courseUrl: String;
    public progress: String;
}