export class Course {
    public courseName: String;
    public courseUrl: String;
    public modules: [CourseModule];
}

class CourseModule {
    public name: String;
    public url: String;
}