# i. Homework description
Design, write codes and report for important units for front-end application and back-end server api.

# ii. Assignment of each member. 
*Minh*:
 
Design, write unit tests and report for back-end Django.

*Tuan*: 

Design, write unit tests and report for front-end Angular application.

*Cem*:

Write unit tests for front-end Angular application.