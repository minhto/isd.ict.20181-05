# Angular Unit tests and Intergration test

In the implementation of this application, the skeleton file for unit test file are generated with the help of [Angular CLI](https://github.com/angular/angular-cli).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Unit test files organization

All the unit test files has the suffix `.spec.ts`, and are placed right next to the module that need to be tested.

List of all written test files:
```
└── angular-src
     └── src
          └── app
                ├── app.component.spec.ts
                |
                ├── components
                |    └── login
                |           └── login.component.spec.ts
                ├── guards
                |    ├── admin.guard.spec.ts
                |    ├── auth.guard.spec.ts
                |    ├── notadmin.guard.spec.ts
                |    └── notauth.guard.spec.ts
                └── services
                        ├── course.service.spec.ts
                        └── user.service.spce.ts
```

There are much more `.ts` file that can have their own `.spec.ts` unit test files, but most of them has similar funcitonality and contains similar function to the test files listed above.

## Adopted testing technique
The *Blackbox Testing Technique* **Decision table** is used here, mainly to provide condition on whether the input data for functions inside the service file is correct or not.

### Description
Test file:
```
└── angular-src
    └── src
        └── app
             └── services
                    └── user.service.spce.ts
```
Sample test cases:

* Register new user

|Register with existing user information|Y|N|
|-|-|-|
|Response result from server: success?|N|Y|

**Sample data**

|Valid?|Username|Email|
|-|-|-|
|Y|tuan|tuannguyendinh224@gmail.com|
|N|test|example@invalidmail.momc|

* Reset password via email address

|Input: valid email?|Y|Y|N|N|
|-|-|-|-|-|
|Existing user with that email?|Y|N|Y|N|
|Response from server: success?|Y|N|N|N|

**Sample data**

|Valid?|Email|
|-|-|
|Y|tuannguyendinh224@gmail.com|
|N|example@invalidmail.momc|

## Run all unit tests
Under `src/node-server`, run
```
node app
```
This will initialize the server-api and allow us to test those test cases that require server access.

Next, under `src/angular-src`, run
```
ng test
```
to execute the unit tests via [Karma](https://karma-runner.github.io).

The result *pass* or *fail* of these test case will be printed out on the terminal, and a debug window will be open at [http://localhost:9876/](http://localhost:9876/) for further inspection.

The result should be similar to the image below.
![Preview](images/tests.png)

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Report and summarize
### Cross checking:
* Tuan examine Cem's unit tests implementation and test cases
* Cem examine Tuan's unit tests implementation and test cases.

The quality and variaty of the provided test cases can still be improved, but given this is the first time we works with unit testing in Typescript, this is pretty much acceptable.
### Summarize
Manually run the provided test cases couple with sample data yield results similar to what are expected.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
