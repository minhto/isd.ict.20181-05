const mongoose = require("mongoose");

const ModuleSchema = mongoose.Schema({
    m_sName: String,
    m_sUrl: String
})

const CourseSchema = mongoose.Schema({
    m_sCourseName: String,
    m_sCourseUrl: String,
    m_acModules: [ModuleSchema]
})

const Course = module.exports = mongoose.model('Course', CourseSchema);

module.exports.GetAllCourses = (callback) => {
    Course.find({}, callback);
}

module.exports.AddCourse = (newCourse, callback) => {
    newCourse.save(callback);
}

module.exports.GetCourse = (courseName, callback) => {
    Course.findOne({m_sCourseName: courseName}, callback);
}

module.exports.UpdateCourseModules = (courseName, courseUrl, modules, callback) => {
    Course.update({m_sCourseName: courseName, m_sCourseUrl: courseUrl}, {m_acModules: modules}, callback);
}