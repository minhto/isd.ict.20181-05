import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  private sOldPassword: String;
  private sNewPassword: String;
  private sConfirmPassword: String;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  private OnChangeSubmit() {
    if (this.sNewPassword != this.sConfirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      this.userService.GetUsername().then(username => {
        this.userService.ChangePassword({username: username, oldPassword: this.sOldPassword, newPassword: this.sNewPassword}).subscribe(data => {
          if (data.success) {
            this.sOldPassword = null;
            this.sNewPassword = null;
            this.sConfirmPassword = null;
            this.flashMessage.show("Success fully changed password. Logging out.", { cssClass: 'alert-success', timeout: 2000 });
            this.userService.Logout();
            this.router.navigate(['/login']);
          } else {
            this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
          }
        })
      })
    }
  }
}
