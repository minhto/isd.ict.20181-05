import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.css']
})
export class LearnComponent implements OnInit {
  private m_uUserProgress: UserProgress;
  private m_cCurrentCourse: Course;
  private m_iCurrentModuleIndex: number;
  private m_iMaxModuleIndex: number;
  private m_iProgressIndex: number;
  private m_content: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.GetUserProgress().then(userProgress => {
      this.m_uUserProgress = <UserProgress>userProgress;
      this.courseService.GetCurrentCourse().then(currentCourse => {
        this.m_cCurrentCourse = <Course>currentCourse;

        this.m_iCurrentModuleIndex = -1;
        this.m_iMaxModuleIndex = -1;
        for (let i = 0; i < this.m_uUserProgress.m_apProgresses.length; i++) {
          if (this.m_cCurrentCourse.m_sCourseUrl == this.m_uUserProgress.m_apProgresses[i].m_sCourseUrl) {
            this.m_iProgressIndex = i;
            for (let j = 0; j < this.m_cCurrentCourse.m_acModules.length; j++) {
              if (this.m_uUserProgress.m_apProgresses[i].m_sProgress == this.m_cCurrentCourse.m_acModules[j].m_sUrl) {
                this.GetModule(j);
                break;
              } 
            }
            break;
          }
        }

        if (this.m_iCurrentModuleIndex == -1) {
          this.m_uUserProgress.m_apProgresses.push({
            m_sCourseUrl: this.m_cCurrentCourse.m_sCourseUrl,
            m_sProgress: this.m_cCurrentCourse.m_acModules[0].m_sUrl
          })
          this.m_iProgressIndex = this.m_uUserProgress.m_apProgresses.length - 1;
          this.GetModule(0);
        }
      })
    })
  }

  private GetModule(moduleIndex: number) {
    if (this.m_iMaxModuleIndex < moduleIndex) {
      this.m_iMaxModuleIndex = moduleIndex;

      this.m_uUserProgress.m_apProgresses[this.m_iProgressIndex].m_sProgress = this.m_cCurrentCourse.m_acModules[moduleIndex].m_sUrl;
      this.UpdateUserProgress().toPromise().then(() => {});
    }
    this.m_iCurrentModuleIndex = moduleIndex;
    this.courseService.GetModule(`${this.m_cCurrentCourse.m_sCourseUrl}/${this.m_cCurrentCourse.m_acModules[moduleIndex].m_sUrl}`).subscribe(content => {
      this.m_content = content;
    })
    this.CloseSidebar();
  }

  private UpdateUserProgress() {
    return this.userService.UpdateUserProgress(this.m_uUserProgress);
  }

  // Toggle sidebar
  private OpenSidebar() {
    document.getElementById("mySidebar").style.display = "block";
  }

  private CloseSidebar() {
    document.getElementById("mySidebar").style.display = "none";
  }

  // Modules navigation
  private OpenPrevModule() {
    this.GetModule(this.m_iCurrentModuleIndex - 1);
    window.scrollTo(0, 0);
  }

  private OpenNextModule() {
    this.GetModule(this.m_iCurrentModuleIndex + 1);
    window.scrollTo(0, 0);
  }
}
