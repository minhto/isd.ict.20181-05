import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private m_uUser: User;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.m_uUser = new User();
  }

  private OnLoginSubmit() {
    this.userService.AuthenticateUser(this.m_uUser).subscribe(data => {
      if (data.success) {
        if (!data.admin) {
          this.userService.StoreUserProgress(data.progress);
        } else {
          this.userService.StoreUserPriviledge(data.admin);
        }
        this.userService.StoreUsername(this.m_uUser.m_sUsername);
        this.flashMessage.show("Logged in", { cssClass: 'alert-success', timeout: 2000 });
        this.router.navigate(['home']);
      } else {
        this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        this.router.navigate(['login']);
      }
    });
  }

}
