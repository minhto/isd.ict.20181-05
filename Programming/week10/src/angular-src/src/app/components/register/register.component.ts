import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private m_uUser: User;
  private m_sConfirmPassword: String;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.m_uUser = new User();
  }

  private OnRegisterSubmit() {
    if (this.m_uUser.m_sPassword != this.m_sConfirmPassword) {
      this.flashMessage.show("Please re-confirm your password", { cssClass: 'alert-danger', timeout: 2000 });
    } else if (!this.ValidateEmail(this.m_uUser.m_sEmail)) {
      this.flashMessage.show("Invalid email address", { cssClass: 'alert-danger', timeout: 2000 });
    } else {
      this.userService.RegisterUser(this.m_uUser).subscribe(data => {
        if (data.success) {
          this.flashMessage.show("Registered success", { cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['login']);
        } else {
          this.flashMessage.show(data.msg, { cssClass: 'alert-danger', timeout: 2000 });
        }
      })
    }
  }

  private ValidateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}