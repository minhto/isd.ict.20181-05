import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';

import { UserProgress, CourseProgress } from '../../classes/userprogress';
import { Course } from '../../classes/course';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private m_uUserProgress: UserProgress;
  private m_sUsername: String;
  private m_isAdmin: Boolean;
  private m_acCourseData: [Course];

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.userService.GetUsername().then(username => {
      this.m_sUsername = <String>username;
      this.m_isAdmin = false;
      if (this.userService.IsAdmin()) {
        this.m_isAdmin = true;
      } else {
        this.userService.GetUserProgress().then(userProgress => {
          this.m_uUserProgress = <UserProgress>userProgress;
          this.courseService.RequestCourses().subscribe(courseData => {
            this.m_acCourseData = <[Course]>courseData;
          })
        })
      }
    })
  }

  private OpenCourse(course: Course) {
    this.courseService.SetCurrentCourse(course);
    this.router.navigate(['learn']);
  }

  private OpenTakenCourse(takenCourse: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (takenCourse.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        this.OpenCourse(this.m_acCourseData[i]);
        break;
      }
    }
  }

  private GetModuleName(course: CourseProgress) {
    for (let i = 0; i < this.m_acCourseData.length; i++) {
      if (course.m_sCourseUrl == this.m_acCourseData[i].m_sCourseUrl) {
        for (let j = 0; j < this.m_acCourseData[i].m_acModules.length; j++) {
          if (course.m_sProgress == this.m_acCourseData[i].m_acModules[j].m_sUrl) {
            return this.m_acCourseData[i].m_acModules[j].m_sName;
          }
        }
        break;
      }
    }
  }
}
