import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { UserService } from './user.service';
import { User } from '../classes/user';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  describe('AuthenticateUser()', () => {
    it('should return authentication result', (done: DoneFn) => {
      const service: UserService = TestBed.get(UserService);
      const user = new User();

      service.AuthenticateUser(user).subscribe(data => {
        expect(data.success).toEqual(true);
        done();
      })
    });
  });

  describe('RegisterUser() with existing account', () => {
    it('should return register result: fail', (done: DoneFn) => {
      const service: UserService = TestBed.get(UserService);
      const user = new User();
      user.m_sUsername = 'tuan';

      service.RegisterUser(user).subscribe(data => {
        expect(data.success).toEqual(false);
        done();
      })
    });
  });

  describe('RegisterUser() with new account', () => {
    it('should return register result: success', (done: DoneFn) => {
      const service: UserService = TestBed.get(UserService);
      const user = new User();
      user.m_sUsername = 'test123123';

      service.RegisterUser(user).subscribe(data => {
        expect(data.success).toEqual(true);
        done();
      })
    });
  });

  describe('ResetUserPassword() with wrong email', () => {
    it('should return result: false', (done: DoneFn) => {
      const service: UserService = TestBed.get(UserService);
      const user = {
        email: 'sample-email@gmail.com'
      }

      service.ResetUserPassword(user).subscribe(data => {
        expect(data.success).toEqual(false);
        done();
      })
    });
  });

  describe('ResetUserPassword() with correct email', () => {
    it('should return result: true', (done: DoneFn) => {
      const service: UserService = TestBed.get(UserService);
      const user = {
        email: 'tuannguyendinh224@gmail.com'
      }

      service.ResetUserPassword(user).subscribe(data => {
        expect(data.success).toEqual(true);
        done();
      })
    });
  });

  describe('IsAdmin()', () => {
    it('should return store isAdmin flag', () => {
      const service: UserService = TestBed.get(UserService);
      expect(service.IsAdmin()).toEqual(!localStorage.getItem('isAdmin') == null);
    });
  });

  describe('IsLoggedIn()', () => {
    it('should return store logged in flag', () => {
      const service: UserService = TestBed.get(UserService);
      expect(service.IsLoggedIn()).toEqual(!localStorage.getItem('username') == null);
    });
  });
});
