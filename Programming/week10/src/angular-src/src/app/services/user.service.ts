import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { User } from '../classes/user';
import { UserProgress } from '../classes/userprogress';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private api = 'http://localhost:3000';
  private m_uUserProgress: UserProgress;
  private m_sUsername: String;

  constructor(
    private http: HttpClient
  ) { }

  public AuthenticateUser(user: User): Observable<any> {
    return this.http.post(`${this.api}/users/authenticate`, user, httpOptions);
  }
  
  public RegisterUser(user: User): Observable<any> {
    return this.http.post(`${this.api}/users/register`, user, httpOptions);
  }

  public ResetUserPassword(user): Observable<any> {
    return this.http.post(`${this.api}/users/resetPassword`, user, httpOptions);
  }

  public StoreUserProgress(userProgress: UserProgress): void {
    this.m_uUserProgress = userProgress;
    localStorage.setItem('userProgress', JSON.stringify(userProgress));
  }

  public StoreUsername(username: String): void {
    this.m_sUsername = username;
    localStorage.setItem('username', JSON.stringify(username));
  }

  public StoreUserPriviledge(isAdmin: Boolean): void {
    localStorage.setItem('isAdmin', JSON.stringify(isAdmin));
  }

  public GetUserProgress() {
    return new Promise((resolve, reject) => {
      if (this.m_uUserProgress) {
        resolve(this.m_uUserProgress);
      } else {
        let savedUserProgress = localStorage.getItem('userProgress');
        if (savedUserProgress !== null) {
          this.m_uUserProgress = JSON.parse(savedUserProgress);
          resolve(JSON.parse(savedUserProgress));
        } else {
          this.Logout();
          reject();
        }
      }
    })
  }

  public GetUsername() {
    return new Promise((resolve, reject) => {
      if (this.m_sUsername) {
        resolve(this.m_sUsername);
      } else {
        let savedUsername = localStorage.getItem('username');
        if (savedUsername !== null) {
          this.m_sUsername = JSON.parse(savedUsername);
          resolve(JSON.parse(savedUsername));
        } else {
          this.Logout();
          reject();
        }
      }
    })
  }

  public IsAdmin(): Boolean {
    return !(localStorage.getItem('isAdmin') == null);
  }

  public UpdateUserProgress(userProgress): Observable<any> {
    this.StoreUserProgress(userProgress);
    return this.http.put(`${this.api}/userProgress/update`, {userID: userProgress.m_sUserID, progresses: userProgress.m_apProgresses}, httpOptions);
  }

  public ChangePassword(user): Observable<any> {
    return this.http.put(`${this.api}/users/changePassword`, user, httpOptions);
  }

  public IsLoggedIn(): Boolean {
    return !(localStorage.getItem('username') == null);
  }

  public Logout() {
    localStorage.clear();
    this.m_uUserProgress = null;
    this.m_sUsername = null;
  }
}
