import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';

import { Observable } from 'rxjs';

import { Course } from '../classes/course';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private api = 'http://localhost:3000';
  private m_acCourses: [Course];
  private m_cCurrentCourse: Course;

  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  public RequestCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.api}/courses/list`);
  }

  public GetCourses() {
    return new Promise((resolve, reject) => {
      if (this.m_acCourses) resolve(this.m_acCourses);
      else {
        this.RequestCourses().subscribe(data => {
          this.m_acCourses = <[Course]> data;
          resolve(data);
        })
      }
    })
  }

  public SetCurrentCourse(course: Course) {
    this.m_cCurrentCourse = course;
    localStorage.setItem('currentCourse', JSON.stringify(course));
  }

  public GetCurrentCourse() {
    return new Promise((resolve, reject) => {
      if (this.m_cCurrentCourse) resolve(this.m_cCurrentCourse);
      else {
        let savedCurrentCourse = localStorage.getItem('currentCourse');
        if (savedCurrentCourse !== null) {
          this.m_cCurrentCourse = JSON.parse(savedCurrentCourse);
          resolve(JSON.parse(savedCurrentCourse));
        } else {
          this.m_acCourses = null;
          this.m_cCurrentCourse = null;
          this.userService.Logout();
          reject();
        }
      }
    })
  }

  public RequestCourseImage(course: String): Observable<any> {
    return this.http.get(`${this.api}/modules/images/${course}`, { responseType: 'text'});
  }

  public GetModule(url: String): Observable<any> {
    return this.http.get(`${this.api}/modules/${url}`, { responseType: 'text'});
  }
}
