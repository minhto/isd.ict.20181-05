import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { CourseService } from './course.service';

describe('CourseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule]
  }));

  it('should be created', () => {
    const service: CourseService = TestBed.get(CourseService);
    expect(service).toBeTruthy();
  });

  describe('RequestCourses()', () => {
    it('should return list of all courses', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.RequestCourses().subscribe(data => {
        expect(data.length).toBeGreaterThan(0);
        done();
      })
    })
  })

  describe('RequestCourseImage', () => {
    it('should return image of requested course', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.RequestCourseImage('html.png').subscribe(data => {
        expect(data).toBeDefined();
        done();
      })
    })
  })

  describe('GetModule', () => {
    it('should return content of requested module', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.GetModule('html/html_basic.asp').subscribe(data => {
        expect(data).toBeDefined();
        done();
      })
    })
  })
});
