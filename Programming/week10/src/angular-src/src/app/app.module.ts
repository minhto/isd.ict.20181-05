import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { FlashMessagesModule } from 'angular2-flash-messages';
import { UserService } from './services/user.service';
import { CourseService } from './services/course.service';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { HomeComponent } from './components/home/home.component';
import { CoursemanagementComponent } from './components/coursemanagement/coursemanagement.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { LearnComponent } from './components/learn/learn.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { ResetcoursesComponent } from './components/resetcourses/resetcourses.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TryitComponent } from './components/tryit/tryit.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ResetpasswordComponent,
    HomeComponent,
    CoursemanagementComponent,
    ChangepasswordComponent,
    LearnComponent,
    NotfoundComponent,
    ResetcoursesComponent,
    SettingsComponent,
    TryitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FlashMessagesModule
  ],
  providers: [
    UserService,
    CourseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
