# Use Case Create Online Saving Account
### Description
A customer can open an Online Saving Account at any bank that is associate with the Internet Banking System.

#
### Primary Actor
* Customer

#
### Secondary Actor
* None

#
### Preconditions
* None.

#
### Postconditions
* None

#
### Main flow
1. The user choose the *Create Online Saving Account* option in the system.
2. The system asks the user to choose the bank that they want to create the Saving Account at.
3. The user chooses a bank from the list.
4. The system asks the user to provide their basic informations.
5. The user enter their informations.
6. The system confirm the informations.
7. The system create a new Online Saving Account.

#
### Alternative Flows
* **6a.** The user enter invalid informations.
    1. The system notify to the user which informations is incorrect and asks them to correct it.
    2. Return to step 4.
* **7a.** The system detect that an Account has already exist for this user.
    1. The system displays that the user already has an account.
    2. The use case ends.

#
### Extend Use Cases
* **8a** The user choose to link their Online Account to their other accounts.
    1. The system asks the user to enter their other account informations.
    2. The user enter their account.
    3. The system verify the informations and register a link between the Online Account and other accounts.
    4. The use case ends.
* **8b** The system asks the user to deposit an arbitrary amount of cash to maintain their account above the mimimum balance.
    1. The user deposits money.
    2. The system verify the money and increase the balance.
    3. The use case ends.