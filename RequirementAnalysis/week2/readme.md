# i. Description of the homework/project

1. Re-draw Use Case Diagram 
2. Do Specification for one common use case and one business use case. 
3. Complete the SRS following template and sample 

# ii. Assignment of each member.

Tuan: 

1. Do Specification for business UC Create Online Saving, common UC Change Password  
2. Complete corresponding parts in the SRS

Minh:

1. Do Specification for common UC Login, business UC External Transfer and Electric Bill
2. Complete corresponding parts in the SRS
   

# iii. review 

1. Minh reviewed Tuan's Specification
2. Tuan reviewed Minh's Specification 