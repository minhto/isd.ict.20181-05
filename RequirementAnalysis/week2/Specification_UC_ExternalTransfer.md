# Use Case External Transfer 
### Description
A customer can transfer money to a destination account which is not in the same bank system with his/her account using the Internet Banking System.

#
### Primary Actor
* Customer

#
### Secondary Actor
* None

#
### Preconditions
* The user successfully logged in to the system.

#
### Postconditions
* None

#
### Main flow
1. The user selects the *external transfer* option in the system.
2. The user provides the *ID number* and the *Bank Name* of the destination account.
3. The user fills in the *amount of money* he/she wants to transfer and some description of the transfer (optional).
4. The user choose the validation form (SMS as default)
5. The Bank takes in the info user provides and return the the information of the destination account and the transaction for the user to check. 
6. After the user accepts the info he/she provides is correct, the system starts processing transaction. 
7. The system sends an OTP code for the user via SMS (default option)
8. Money transfered and the system sends back the online receipt for the user to validate transfer success. 

#
### Alternative Flows
* **5a.** The user enter invalid destination account informations.
    1. The system notify to the user that it can not detect the destination account.
    2. Return to step 2.
* **6a.** The user transfer money exceed the amount available in the account or the maximum amount can be transfered at one time.
    1. The system notifies the user does not have enough money (or exceed amount thresshold) to execute this transfer.
    2. Return to step 3. 
* **7a.** OTP code never arrives 
    1. User requests another OTP codes sent if he/she has to wait for a long time. 
    2. Return to step 7. 

