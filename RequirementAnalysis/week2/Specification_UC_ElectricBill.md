# Use Case Electric Bill payment 
### Description
A customer can pay electric bill monthly via the Internet Banking System.

#
### Primary Actor
* Customer

#
### Secondary Actor
* None

#
### Preconditions
* The user successfully logged in to the system.

#
### Postconditions
* None

#
### Main flow
1. The user selects the *Pay Bills* option in the system and then choose *Electric Bill*. 
2. The user provides the *ID number* register with the Electric Service Provider. 
3. The system check the ID number and returns the customer's info (extract from the electric service's side) and the amount of money needed to be paid for this month. 
4. The user, if choose to pay the bill, proceed to next step to receive the OTP code to validate the action.   
5. The Bank executes the payment and returns E-receipt for user.
#

### Alternative Flows
* **3a.** The user enter invalid account information.
    1. The system notify to the user that it can not detect the ID of the electric service's account.
    2. Return to step 2.
* **3a.** It is not time to pay the bill yet 
    1. The system notifies the user that the service has not charged fee for this month yet.
    2. The process ends here.
* **7a.** OTP code never arrives 
    1. User requests another OTP codes sent if he/she has to wait for a long time. 
    2. Return to step 4. 

