# some terminologies used in this problem

1. Bank consortium: A consortium bank is a subsidiary bank, which numerous other banks create. These banks might create a consortium bank to fund a specific project or to execute a specific deal  

2. Internal transfer: transfer between accounts in the same bank 

3. External transfer: transfer between accounts from different banks 

