# i. Description of the homework/project

1. Choose software architectual patterns for front-end and back-end of your project (framework + programming languages)

2. Describe software architecture of your project (implementation)

3. Re-design all assigned use cases and analysis class diagrams 

# ii. Assignment of each member.

*Tuan*: 

Re-draw Sequence Diagram and Activity Diagram for use-cases:

1. Add Course 
2. Change Password 
3. Admin create challenge for user 
4. Admin modify course 
5. Admin Review challenge created by user 

Add architectual pattern mark down file of front-end 

*Minh*: 

Re-draw Sequence Diagram and Activity Diagram for use-cases:

1. Login
3. Register Course 
4. Reset Course 
5. User Create Challenge 

Add architectual pattern mark down file of back end 


*Note*: this week Cem had some family business so he did not submit the homework, he already informed the lecturer about this. 