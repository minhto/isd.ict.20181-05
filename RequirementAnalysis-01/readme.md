# i. Description of the homework/project

1. Analyse the Internet Banking System's main actors, their roles, functions. 

2. Draw use case and activity diagram for this system

3. Study relationships between actors, use cases 

4. Define some common terminologies in the banking system 

# ii. Assignment of each member.

Tuan: 

1. draw activity diagram 

2. study relationships between use cases, actors 

Minh:

1. draw use case diagram 

2. define terminologies 

# iii. review 

1. Minh reviewed Tuan's diagram 

2. Tuan reviewed Minh's diagram 

