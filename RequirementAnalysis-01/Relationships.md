# Relationships between actors
### Generalization
Actor generalization refers to the relationship which can exist between two actors and which shows that one actor (descendant) inherits the role and properties of another actor (ancestor). The generalization relationship also implies that the descendant actor can use all the use cases that have been defined for its ancestor.

**_Example_**: In an air travel business system, both a **Business Traveler** and a **Tourist** are generalized as **Passengers**.  The fact that they are passengers allow them to have common behavior such as *Buy Ticket* but the fact that they are separate actors implies they can also have differences.  The **Business Traveler** might be able to *Redeem Business Ticket* while the **Tourist** cannot.
#
### Association
When an actor can not interacts directly with an use case, this actor form an Association relationship with the actor that has direct relation with said use case, and then had that actor interact with the desired use case in place on them.

**_Example_**: In a library, a **Student** can not borrow the books directly, they have to inform the **Librarian**, had them *Register* the books and bring the books back to the **Student**.

# Relationships between use cases
### Include
The include-relationship connects a base use case to an inclusion use case. The inclusion use case is always abstract. It describes a behavior segment that is inserted into a use-case instance that is executing the base use case.

We can use the include-relationship to:
* Factor out behavior from the base use case that is not necessary for the understanding of the primary purpose of the use case, only the result of it is important.
* Factor out behavior that is in common for two or more use cases.

**_Example_**: In an ATM system, the use cases *Withdraw Cash*, *Deposit Cash*, and *Transfer Funds* all need to include how the **Customer** is identified to the system. This behavior can be extracted to a new inclusion use case called *Identify Customer*, which the three base use cases include.
#
### Extend
Extend is a directed relationship that specifies how and when the behavior defined in the *extending* use case can be inserted into the behavior defined in the *extended* use case.

*Extended* use case is independent from the *extending* use case. *Extended* use case typically defines optional behavior that is not necessarily meaningful by itself. The extend relationship is owned by the *extending* use case. The same *extending* use case can extend more than one use case, and *extending* use case may itself be *extended*.

**_Example_**: In an ATM machine, the use case *Eat card* can be extended from the use case *Verify card*. The extending use case *Verify card* will lead to the extended use case *Eat card* if only the **User** failed to enter their PIN 3 times or the card is marked as a stolen card.

#
### Generalization
Generalization is used when two or more use cases have commonalities in behavior, structure, and purpose. When this happens, we can describe the shared parts in a new, often abstract, use case, that is then specialized by child use cases.

A parent use case may be specialized into one or more child use cases that represent more specific forms of the parent. Neither parent nor child is necessarily abstract, although the parent in most cases is abstract. A child inherits all structure, behavior, and relationships of the parent. Children of the same parent are all specializations of the parent.

**_Example_**: In a system, when the user proceed with payment method, there are **Purchase by card** and **Purchase by cash** use cases, which share a lot of commonalities. A general use case **Purchase** then is defined.