# i. Description of the homework/project

Base on the SRS of the previous week, each member continue to  make  architectural  design  for  assigned  use  cases (3  use  cases  /  member) and draws the corresponding *Sequence Diagram* and *Communication Diagram* for those use cases.
# ii. Assignment of each member.

*Tuan*: Draw Sequence Diagram and Communication Diagram for use-cases:

1. Use case Change password.
2. Use case Purchase courses from bank account.
3. Use case Transfer money from bank account to application account.

*Minh*: Draw Sequence Diagram and Communication Diagram for use-cases:

1. Use case Buy course for friend.
2. Use case Purchase courses from application account.
3. Use case Login.