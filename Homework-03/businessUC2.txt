Business Use Case 
- Link account to bank account (Cem)
- Purchase courses using bank account (Tuan)
- Purchase courses directly using account (Minh)
- Refund when canceling courses (Cem)
- Add more money to account (Tuan)
- Present courses to friends (Minh)

Common Use Case 
- Login (Minh)
- Change Pass (Tuan)
- Register to Course (Cem)
