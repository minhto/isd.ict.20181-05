# Use Case Transfer Money From Bank Account
### Description
In order to purchase new courses, the user can either purchase directly from their bank account (which is linked with the system account), or transfer from their bank account to the system account, and then purchase using the system account. In this use case, the user transfer money from their bank account to their system account.

#
### Primary Actor
* User

#
### Secondary Actor
* The bank system

#
### Preconditions
* The user's account is linked with their bank accounts.

#
### Postconditions
* The money is transfered from the bank account to the user' system account.

#
### Main flow
1. The user chooses the *Transfer Money From Bank Account* function.
2. The system asks the user to enter the amount of money they wish to transfer.
3. The user enter the amount of money.
4. The system sends request to the banking system.
5. The bank system confirms the transaction.
6. The system displays the successful result to the user.

#
### Alternative Flows
* **2a.** The user' system account is not linked with any bank account.
    1. The system displays the message to the user.
    2. The use case ends.
* **5a.** The user's bank account balance is not sufficient to proceed the payment.
    1. The bank system notify the system that the payment can not be proceeded.
    2. The system displays the message to the user.
    3. The use case ends.