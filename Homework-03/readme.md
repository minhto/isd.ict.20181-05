# i. Description of the homework/project

Complete the SRS of our project: 
1. Use Case Diagram 
2. Glossary: Define common terminologies in the system. 
3. Activity Diagrams 
4. Do specifications
# ii. Assignment of each member.

Tuan: 

1. Draw Activity Diagrams and do Specification for Purchase Courses using bank account, Transfer Money to account, common UC Change Password  
2. Finish corresponding part in SRS 
Minh:

1. Draw Activity Diagrams and do Specification for Purchase Course directly using account, Give courses to friends, common UC Login
2. Finish corresponding part in SRS 
Cem: 

1. Draw Activity Diagrams and do Specification for Refund when cancelling courses, Link account to bank account, common UC Register to Course
2. Finish corresponding part in SRS 
    

# iii. review 

1. Minh reviewed Tuan's part
2. Tuan reviewed Cem's part 
3. Cem reviewed Minh's part