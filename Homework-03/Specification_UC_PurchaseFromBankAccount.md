# Use Case Purchase Course From Bank Account
### Description
In order to purchase new courses, the user can either purchase directly from their bank account (which is linked with the system account), or transfer from their bank account to the system account, and then purchase using the system account. In this use case, the user purchase the course directly from their bank account.

#
### Primary Actor
* User

#
### Secondary Actor
* The bank system

#
### Preconditions
* The user's account is linked with their bank accounts.

#
### Postconditions
* New courses are registered for the user.

#
### Main flow
1. The user chooses the *Purchase Courses* function.
2. The user chooses the courses that they wish to purchase.
3. The system asks the user to choose their payment method.
4. The user chooses to pay via their bank account.
5. The system sends request to the bank system to verify the payment.
6. The bank system confirms the payment.
7. The system registers new courses to the user.

#
### Alternative Flows
* **3a.** The courses that the user wished to purchase are already owned by them.
    1. The system displays the message to the user.
    2. Return to step 2.
* **4a.** The user's account doesn't link with any bank accounts.
    1. The system then asks the user to link their account with a bank account.
    2. The use case ends.
* **6a.** The user's bank account balance is not sufficient to proceed the payment.
    1. The bank system notify the system that the payment can not be proceeded.
    2. The system displays the message to the user.
    3. The use case ends.