package Classes.UserProgress;

import Classes.Array;
import UserService.UserService;
import java.util.Collection;
import Classes.User.User;

public class UserProgress {

	public Array[] badges;

	public int exp;

	public Array[] progresses;

	public string userID;

	private CourseProgress courseProgress;

	private UserService userService;

	private Collection<CourseProgress> courseProgress;

	private User user;

}
