# i. Description of the homework/project

1. Each member programs (write codes) for one more use case following the chosen frameworks/ architectual patterns.
2. Each member design class (attributes, operations, methods)
3. Each team uses packages to manage classes, design relationship between classes, create class diagrams, using tool to compare codes 


# ii. Assignment of each member.
*Tuan*: 
* Design classes
* Program Use Case Code PlayGround
* Review and compare exported codes with source codes

*Minh*: 
* Design relationship between classes
* Program Use Case Reset Password 

*Cem*
* Design class packages 
* Program Use Case Settings
* Export code from class diagrams

# iii. File organazation
* Source code
```
/Project/week7
```
* Detail Design (asta file)
```
/DetailedDesign/week7/Design-Classes.asta
```
* Detail Design (exported images)
```
/DetailedDesign/week7/Design-Classes/
```
* Exported .java code from asta
```
/DetailedDesign/week7/Exported Code/
```
* Review exported code and comparison
```
/DetailedDesign/week7/Exported Code/Review.md
```