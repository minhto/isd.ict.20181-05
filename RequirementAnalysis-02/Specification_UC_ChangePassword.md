# Use Case Change Password
### Description
Both Customers or Bank officers can change the password of their accounts. In this use case, the user first need to *Log in* to the system, choose the *Change Password* functionality, then enter a new valid password that is accepted by the system.

#
### Primary Actor
* Customer
* Bank Officer

#
### Secondary Actor
* None

#
### Preconditions
* The user need to have a valid account.

#
### Postconditions
* The new password is different from the old password.
* The new password contains only characters that are recognized by the system.

#
### Main flow
1. The **User** (Customer or Bank officer) *Login* to the system with their account.
2. The system verify the account.
3. The system display the account informations.
4. The user choose the *Change password* function.
5. The system prompt the user to enter their current password.
6. The user enter their current password.
7. The system prompt the user to enter their new password.
8. The user enter their new password.
9. The system ask the user to re-enter their new password.
10. The user re-enter the new password.
11. The system register the user's new password.

#
### Alternative Flows
* **2a.** The user enter an invalid account.
    1. The system notify the user that the account information is wrong.
    2. Return to step 1.
* **2b.** The user has entered an invalid account 3 times.
    1. The system display to the user that they have exceeded the maximum login attempts.
    2. The system notify its administrator.
    3. The use case ends.
* **3a.** The user choose to logout of the system.
    1. The use case ends.
* **6a.** The user fail to enter their current password.
    1. The system display that the entered password is not the current password.
    2. Return to step 3.
* **6b.** The user choose to cancel their password changing process.
    1. Return to step 3.
* **8a.** The user enter the same password as their old password.
    1. The system notify to the user that their new password is the same as their old one.
    2. Return to step 7.
* **8b.** The user choose to cancel their password changing process.
    1. Return to step 3.
* **10a.** The user fail to re-enter their new password.
    1. The system notify to the user that the password that has just entered is not the same as the new password.
    2. Return to step 9.
* **10b.** The user choose to cancel their password changing process.
    1. Return to step 3.