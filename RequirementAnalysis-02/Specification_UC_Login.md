# Use Case Login
### Description
Both Customers or Bank officers have to login before using any service or perform any task. In this use case, *Log in* is the first task needed to be done. Specifically, users have to fill in their username (phone number or personal email may do as well), password (PIN code) for the system to check. 

#
### Primary Actor
* Customer
* Bank Officer

#
### Secondary Actor
* None

#
### Preconditions
* The user need to sign up for a valid account (this task can be done online or directly at any bank office)
* The user has to provide correct persional information for the bank to manage account
#

### Postconditions
* None

#
### Main flow
1. The **User** (Customer or Bank officer) opens the internet banking app or go the website.
2. Fill in the 2 fields username and password (can use fingerprint instead of filling in password for supported devices) 
3. Waiting for the system to validate information.
4. The system grants the user the right to access. 

#
### Alternative Flows
* **2a.** The user enter an invalid account.
    1. The system notify the user that the account information is wrong.
    2. Return to step 2.
* **2b.** The user has entered an invalid account 3 times.
    1. The system display to the user that they have exceeded the maximum login attempts.
    2. The system notify its administrator.
    3. The use case ends.
* **2c.** The user forgets his/her username or password.
    1. The system directs the user to another window. 
    2. The user has to fill in his/her correct information (phone number/email/citizen ID) for the bank to validate 
    3. After validation, new password/username will be sent to user via his/her email
    4. User uses this new password to access the service 
* **2d.** The user does not have an account and want to sign up for one. 
    1. The system directs the user to the sign up window. 
    2. The user fills in all the personal information required (The user needs to have the bank account first before signing up for the online service of that bank)
    3. The bank checks and validates the information and create an account for users. 