# Review
### Tuan's review about Minh's UC specifications
## Login
* In the main flow, the step 1 is redundant and can be removed.
* Step 3 does not follow the convention: *Actor/Entity* + *Action*. Should be: *The system verifies user informations*.
* Step 4 should be *The system displays the result to user*.
* Alternative flow *2d* is out of the scope of the use case, since we already have the precondition that the user has an account.
## External Trasfer
* There should be a secondary actor: *The system of the external bank*
* Step 6 should be more specific, like: *The system verify the account balance*
## Pay Electric Bill
* Alternative flow 7a: Mistyped, should be 5a.

### Minh's review on Tuan's UC specifications 
## Create Online Saving 
* In the alternative flow 6, we can add another flow when user enter invalid information more than 3 times, then the use case is terminated immediately. 
* In the alternative flow 2, the step Choose bank to create account may be redundant, since we by default create the online saving account for the bank we use the service

## Change Password 
* In the alternavive flow 8, should notify when the password user type in is too simple or predictable. 

