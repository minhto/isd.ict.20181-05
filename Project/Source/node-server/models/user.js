const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    m_sUsername: String,
    m_sPassword: String,
    m_sEmail: String,
    m_isAdmin: Boolean
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.GetAllUsers = (callback) => {
    User.find({}, callback);
}

module.exports.AddUser = (newUser, callback) => {
    newUser.save(callback);
}

module.exports.AuthenticateUser = (username, password, callback) => {
    User.findOne({m_sUsername: username, m_sPassword: password}, callback);
}

module.exports.GetUserWithUsername = (username, callback) => {
    User.findOne({m_sUsername: username}, callback);
}

module.exports.ChangePassword = (username, newPassword, callback) => {
    User.update({m_sUsername: username}, {m_sPassword: newPassword}, callback);
}

module.exports.GetUserWithEmail = (email, callback) => {
    User.findOne({m_sEmail: email}, callback);
}