const express = require("express");
const router = express.Router();
const Course = require("../models/course");

router.get('/list', (req, res, next) => {
    Course.GetAllCourses((err, courses) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(courses);
    })
})

router.post('/register', (req, res, next) => {
    let newCourse = new Course({
        m_sCourseName: req.body.m_sCourseName,
        m_sCourseUrl: req.body.m_sCourseUrl,
        m_acModules: req.body.m_acModules
    });

    Course.AddCourse(newCourse, (err) => {
        if (err)
            res.json({
                success: false,
                msg: `Error occurred: ${err}`
            });
        else
            res.json({
                success: true,
                msg: "Course resgitered successful"
            });
    })
})

router.get('/:courseName', (req, res, next) => {
    Course.GetCourse(req.params.courseName, (err, course) => {
        if (err) {
            res.send(`Error occurred: ${err}`);
            next();
        }
        res.json(course);
    })
})

router.put('/update', (req, res, next) => {
    Course.UpdateCourseModules(req.body.m_sCourseName, req.body.m_sCourseUrl, req.body.m_acModules, (err) => {
        if (err) {
            res.json({ success: false, msg: "Failed to update course modules"})
        } else {
            res.json({ success: true, msg: "Successfully update course modules" })
        }
    })
})

module.exports = router;