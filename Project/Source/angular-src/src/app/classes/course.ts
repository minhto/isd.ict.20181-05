export class Course {
    public m_sCourseName: String;
    public m_sCourseUrl: String;
    public m_acModules: [CourseModule];
}

class CourseModule {
    public m_sName: String;
    public m_sUrl: String;
}