import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { NotauthGuard } from './guards/notauth.guard';
import { AdminGuard } from './guards/admin.guard';
import { NotadminGuard } from './guards/notadmin.guard';

import { NotfoundComponent } from './components/notfound/notfound.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { HomeComponent } from './components/home/home.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { LearnComponent } from './components/learn/learn.component';
import { TryitComponent } from './components/tryit/tryit.component';
import { CoursemanagementComponent } from './components/coursemanagement/coursemanagement.component';
import { ResetcoursesComponent } from './components/resetcourses/resetcourses.component';

const routes: Routes = [
  {path:'404', component: NotfoundComponent},
  {path:'home', component: HomeComponent, canActivate: [AuthGuard] },
  {path:'resetcourses', component: ResetcoursesComponent, canActivate: [AuthGuard] },
  {path:'login', component: LoginComponent, canActivate: [NotauthGuard] },
  {path:'register', component: RegisterComponent, canActivate: [NotauthGuard] },
  {path:'resetpassword', component: ResetpasswordComponent, canActivate: [NotauthGuard] },
  {path:'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
  {path:'learn', component: LearnComponent, canActivate: [AuthGuard] },
  {path:'tryit', component: TryitComponent, canActivate: [AuthGuard] },
  {path:'coursemanagement', component: CoursemanagementComponent, canActivate: [AdminGuard] },
  {path: '**', redirectTo: '404'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}