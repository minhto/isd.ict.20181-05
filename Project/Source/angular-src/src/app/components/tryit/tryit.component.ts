import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../services/course.service';

declare var editor: any;

@Component({
  selector: 'app-tryit',
  templateUrl: './tryit.component.html',
  styleUrls: ['./tryit.component.css']
})
export class TryitComponent implements OnInit {

  constructor(
    private courseService: CourseService
  ) { }

  ngOnInit() {
    let filename = document.location.href.split('=')[1];
    if (filename !== undefined) {
      let filepath = `tryit/${filename.split('_')[0].slice(3)}/${filename}`;
      this.courseService.GetModule(filepath).subscribe(res => {
        this.InitiateEditor('`' + res + '`');
      })
      // .catch(err => {
      //   // Handle: file does not exist
      //   console.log("File does not exists");
      //   this.InitiateEditor(`''`);
      // })
    } else {
      // Empty filename
      this.InitiateEditor(`''`);
    }
  }

  private InitiateEditor(value) {
    let s = document.createElement('script');
    s.textContent = `
      let editor = CodeMirror(document.getElementById('code-editor'), {
        value: ${value},
        mode: "text/html",
        theme: "night",
        lineNumbers: true,
        extraKeys: { "Ctrl-Space": "autocomplete" },
        autoCloseBrackets: true
      });
      editor.setSize("height", "100%");

      let run = function () {
          let content = editor.getValue();
          let iframe = document.getElementById('output');
          iframe = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
          iframe.document.open();
          iframe.document.write(content);
          iframe.document.close();
          return false;
      }

      run();
    `;
    document.body.appendChild(s);
  }

  private ChangeEditorTheme() {
    let currentTheme = editor.getOption('theme');
    if (currentTheme == 'night') editor.setOption('theme', 'neo');
    else editor.setOption('theme', 'night');
  }

  private ChangeEditorOrientation() {
    let row = document.getElementById('row');
    let col1 = document.getElementById('col1');
    let col2 = document.getElementById('col2');

    if (row.classList.contains('row')) {
      // Editor are being display in a row
      row.classList.remove('row');
      col1.classList.remove('column');
      col1.classList.add('row2');
      col2.classList.remove('column');
      col2.classList.add('row2');
    } else {
      // Editor are being display in a column
      row.classList.add('row');
      col1.classList.add('column');
      col1.classList.remove('row2');
      col2.classList.add('column');
      col2.classList.remove('row2');
    }
  }
}
