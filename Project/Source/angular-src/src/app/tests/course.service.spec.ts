import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { CourseService } from '../services/course.service';

describe('Course Service test cases', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule]
  }));

  it('Course service creation success', () => {
    const service: CourseService = TestBed.get(CourseService);
    expect(service).toBeTruthy();
  });

  describe('Request list of all courses use case', () => {
    it('should return list of all courses', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.RequestCourses().subscribe(data => {
        expect(data.length).toBeGreaterThan(0);
        done();
      })
    })
  })

  describe('Request course image use case', () => {
    it('should return image of requested course', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.RequestCourseImage('html.png').subscribe(data => {
        expect(data).toBeDefined();
        done();
      })
    })
  })

  describe('Request module content use case', () => {
    it('should return content of requested module', (done: DoneFn) => {
      const service: CourseService = TestBed.get(CourseService);
      service.GetModule('html/html_basic.asp').subscribe(data => {
        expect(data).toBeDefined();
        done();
      })
    })
  })
});
